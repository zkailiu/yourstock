package com.zkliu.android.yourstock.data.source.users;

import android.content.Context;

import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;
import com.zkliu.android.yourstock.data.source.users.local.UsersLocalDataSource;
import com.zkliu.android.yourstock.data.source.users.remote.UsersRemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by zekail on 12/7/2017.
 */
@Module
public class UsersRepositoryModule {

    @Singleton
    @Provides
    @Local
    UsersDataSource provideUsersLocalDataSource(Context context){
        return new UsersLocalDataSource(context);
    }

    @Singleton
    @Provides
    @Remote
    UsersDataSource provideUsersRemoteDataSource(){
        return new UsersRemoteDataSource();
    }
}
