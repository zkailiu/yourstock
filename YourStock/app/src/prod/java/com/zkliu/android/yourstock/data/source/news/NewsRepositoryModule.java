package com.zkliu.android.yourstock.data.source.news;

import android.content.Context;

import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;
import com.zkliu.android.yourstock.data.source.news.local.NewsLocalDataSource;
import com.zkliu.android.yourstock.data.source.news.remote.NewsRemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by zekail on 25/4/2017.
 */
@Module
public class NewsRepositoryModule {

    @Singleton
    @Provides
    @Local
    NewsDataSource provideNewsLocalDataSource(Context context){
        return new NewsLocalDataSource(context);
    }

    @Singleton
    @Provides
    @Remote
    NewsDataSource provideNewsRemoteDataSource(){
        return new NewsRemoteDataSource();
    }
}
