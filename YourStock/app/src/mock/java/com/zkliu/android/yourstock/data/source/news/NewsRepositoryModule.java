package com.zkliu.android.yourstock.data.source.news;

import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;
import com.zkliu.android.yourstock.data.source.news.local.NewsLocalDataSource;
import com.zkliu.android.yourstock.data.source.news.remote.FakeNewsRemoteDataSource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Created by zekail on 25/4/2017.
 */
@Module
abstract class NewsRepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract NewsDataSource provideNewsLocalDataSource(NewsLocalDataSource dataSource);

    @Singleton
    @Binds
    @Remote
    abstract NewsDataSource provideNewsRemoteDataSource(FakeNewsRemoteDataSource dataSource);

}
