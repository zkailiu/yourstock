package com.zkliu.android.yourstock.data.source.news.remote;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by zekail on 25/4/2017.
 */

public class FakeNewsRemoteDataSource implements NewsDataSource {

    private static final Map<String, NewsDTO> NEWS_SERVICE_DATA = new LinkedHashMap<>();

    @Inject
    public FakeNewsRemoteDataSource(){};

    @Override
    public Observable<List<NewsDTO>> getHeadLindNewsList(){
        return null;
    }
}
