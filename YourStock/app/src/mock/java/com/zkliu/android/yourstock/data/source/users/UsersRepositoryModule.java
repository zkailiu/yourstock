package com.zkliu.android.yourstock.data.source.users;

import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;
import com.zkliu.android.yourstock.data.source.news.local.NewsLocalDataSource;
import com.zkliu.android.yourstock.data.source.news.remote.FakeNewsRemoteDataSource;
import com.zkliu.android.yourstock.data.source.users.local.UsersLocalDataSource;
import com.zkliu.android.yourstock.data.source.users.remote.FakeUsersRemoteDataSource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Created by zekail on 25/4/2017.
 */
@Module
abstract class UsersRepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract NewsDataSource provideUsersLocalDataSource(UsersLocalDataSource dataSource);

    @Singleton
    @Binds
    @Remote
    abstract NewsDataSource provideUsersRemoteDataSource(FakeUsersRemoteDataSource dataSource);

}
