package com.zkliu.android.yourstock.mvp.main.news.sub;

import com.zkliu.android.yourstock.app.StockApplication;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;

import javax.inject.Inject;

/**
 * Created by zekail on 4/5/2017.
 */

public class NewsSubPresenterFactory implements BasePresenterFactory<NewsSubPresenter>{


    private final NewsSubFragment newsSubFragment;
    public NewsSubPresenterFactory(NewsSubFragment mNewsFragment){
        newsSubFragment = mNewsFragment;
    }

    @Inject
    NewsSubPresenter newsSubPresenter;

    @Override
    public NewsSubPresenter create(){
        DaggerNewsSubComponent.builder()
                .newsSubModule(new NewsSubModule(newsSubFragment))
                .newsRepositoryComponent(
                        ((StockApplication)newsSubFragment.getActivity().getApplication()).getNewsRepositoryComponent()
                )
                .build().inject(this);
        return newsSubPresenter;
    }
}
