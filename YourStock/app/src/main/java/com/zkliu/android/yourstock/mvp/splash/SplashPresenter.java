package com.zkliu.android.yourstock.mvp.splash;

import android.support.annotation.NonNull;

import javax.inject.Inject;


/**
 * Created by liuzekai on 2016/12/28.
 */

public class SplashPresenter implements SplashContract.Presenter {

    @NonNull
    private SplashContract.View mSplashView;

    @Inject
    SplashPresenter(SplashContract.View splashView){
        mSplashView = splashView;
    }


    private boolean isStarted = false;
    private boolean isAnimationCancel = false;
    @Override
    public void start(){
        if(!isStarted && !isAnimationCancel){
            mSplashView.startBottomAnimation();
            isStarted = true;
        }
    }

    @Override
    public void animationFinished(int stepID){
        int animationStep = stepID;
        if(mSplashView != null && !isAnimationCancel){
            switch (animationStep){
                case SplashContract.ANIMATIONSTEPONE:
                    mSplashView.startBezierTransAnimation();
                    break;
                case SplashContract.ANIMATIONSTEPTWO:
                    mSplashView.startBezierRotateAnimation();
                    break;
                case SplashContract.ANIMATIONSTEPTHREE:
                    mSplashView.startShowCenterLogoCover();
                    break;
                case SplashContract.ANIMATIONSTEPFOUR:
                    mSplashView.startBottomAnimationReversal();
                    break;
            }
        }
    }

    @Override
    public void onViewAttached(SplashContract.View view){
        this.mSplashView = view;
    }

    @Override
    public void onViewDetached() {
        isAnimationCancel = true;
        mSplashView.cancelCurrentAnimation();
        mSplashView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }
}
