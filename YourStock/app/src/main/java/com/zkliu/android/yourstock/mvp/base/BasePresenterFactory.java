package com.zkliu.android.yourstock.mvp.base;

/**
 * Created by liuzekai on 2017/1/10.
 */

public interface BasePresenterFactory<T extends BasePresenter> {

    T create();
}
