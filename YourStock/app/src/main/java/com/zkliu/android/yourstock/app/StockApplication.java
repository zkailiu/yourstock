package com.zkliu.android.yourstock.app;

import android.app.Application;
import android.content.Context;

import com.zkliu.android.yourstock.data.source.news.DaggerNewsRepositoryComponent;
import com.zkliu.android.yourstock.data.source.news.NewsRepositoryComponent;
import com.zkliu.android.yourstock.data.source.users.DaggerUsersRepositoryComponent;
import com.zkliu.android.yourstock.data.source.users.UsersRepositoryComponent;

/**
 * Created by liuzekai on 2016/12/26.
 */

public class StockApplication extends Application {

    private static Context context;
    private NewsRepositoryComponent mNewsRepositoryComponent;
    static private UsersRepositoryComponent mUsersRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        StockApplication.context = getApplicationContext();
        mNewsRepositoryComponent = DaggerNewsRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
        mUsersRepositoryComponent = DaggerUsersRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public NewsRepositoryComponent getNewsRepositoryComponent(){
        return mNewsRepositoryComponent;
    }

    static public UsersRepositoryComponent getUsersRepositoryComponent(){
        return mUsersRepositoryComponent;
    }

    public static Context getAppContext() {
        return StockApplication.context;
    }
}
