package com.zkliu.android.yourstock.data.source.users.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.zkliu.android.yourstock.data.source.users.UsersDataSource;
import com.zkliu.android.yourstock.retrofit.RetrofitClient;
import com.zkliu.android.yourstock.retrofit.RetrofitService;

import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by zekail on 12/7/2017.
 */
@Singleton
public class UsersRemoteDataSource implements UsersDataSource{

    public UsersRemoteDataSource(){}

    @Override
    public Observable<Bitmap> getCaptcha(){
        return RetrofitClient.getInstance()
                .create(RetrofitService.class)
                .getCaptcha()
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<ResponseBody>, Bitmap>() {
                    @Override
                    public Bitmap apply(@io.reactivex.annotations.NonNull Response<ResponseBody> response) throws Exception {
                        Bitmap bm = BitmapFactory.decodeStream(response.body().byteStream());
                        return bm;
                    }
                });
    }

    @Override
    public Observable<Integer> register(String mail, String password, String captcha, String name){
        Log.i("register",mail+" "+password+" "+ captcha+" "+name);
        return RetrofitClient.getInstance()
                .create(RetrofitService.class)
                .register(mail, password, captcha, name)
                .subscribeOn(Schedulers.io())
                .map(registerGson -> registerGson.getResult_code());
    }
}
