package com.zkliu.android.yourstock.mvp.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.base.backpress.FragmentBackHandler;
import com.zkliu.android.yourstock.util.ActivityUtils;
import com.zkliu.android.yourstock.util.Utils;

import java.util.regex.Pattern;

import static com.zkliu.android.yourstock.mvp.login.LoginActivity.LOGIN_FRAG;
import static com.zkliu.android.yourstock.mvp.login.LoginActivity.REGISTER_FRAG;

/**
 * Created by liuzekai on 06/07/2017.
 */

public class RegisterFragment extends BaseFragment<LoginPresenter, LoginContract.View> implements LoginContract.View,FragmentBackHandler{

    Toolbar mToolbar;
    EditText mailEdit;
    EditText passEdit;
    private boolean isShowPassword;
    TextView passText;
    EditText nameEdit;
    EditText captchaEdit;
    ImageView captchaImg;
    Button registerBtn;
    ProgressBar mProgressBar;

    private LoginContract.Presenter mPresenter;
    private boolean hasLoaded;

    public RegisterFragment(){
    }

    public static RegisterFragment newInstance(){
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.frag_register,container,false);
        mToolbar = (Toolbar)root.findViewById(R.id.register_toolbar);
        mToolbar.setNavigationOnClickListener(view->{
            getActivity().onBackPressed();
        });
        mToolbar.setTitle(R.string.register);

        mailEdit = (EditText)root.findViewById(R.id.mail);
        mailEdit.addTextChangedListener(mTextWatcher);

        passText = (TextView)root.findViewById(R.id.password_remind);
        passEdit = (EditText)root.findViewById(R.id.password);
        passEdit.addTextChangedListener(mTextWatcher);
        passEdit.setOnFocusChangeListener((view,b)->{
            if (b){
                passText.setVisibility(View.VISIBLE);
            }else {
                passText.setVisibility(View.GONE);
                isShowPassword = false;
                passEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                passEdit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_outline, 0, R.drawable.ic_eye_off_outline, 0);
            }
        });
        passEdit.setOnTouchListener((view,event)->{
            Drawable drawable = passEdit.getCompoundDrawables()[2];
            if (drawable == null)
                return false;
            if (event.getAction() == MotionEvent.ACTION_UP){
                if(event.getRawX() >= (passEdit.getRight() - passEdit.getCompoundDrawables()[2].getBounds().width())) {
                    isShowPassword = !isShowPassword;
                    if (isShowPassword){
                        passEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        passEdit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_outline, 0, R.drawable.ic_eye_open_offline, 0);
                    }else {
                        passEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        passEdit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_outline, 0, R.drawable.ic_eye_off_outline, 0);
                    }
                    return false;
                }
                return false;
            }
            return false;
        });

        nameEdit = (EditText)root.findViewById(R.id.username);
        nameEdit.addTextChangedListener(mTextWatcher);
        registerBtn = (Button)root.findViewById(R.id.register);
        if(mailEdit.getText().toString().equals("")
                || passEdit.getText().toString().equals("")
                || nameEdit.getText().toString().equals("")
                || captchaEdit.getText().toString().equals("")){
            registerBtn.setEnabled(false);
        }
        registerBtn.setOnClickListener(v -> {
            if(isInputValid()){
                mPresenter.register(mailEdit.getText().toString().trim(),passEdit.getText().toString().trim(),
                        captchaEdit.getText().toString().trim(),nameEdit.getText().toString().trim());
                InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(registerBtn.getWindowToken(), 0);
                mProgressBar.setVisibility(View.VISIBLE);
            }

        });
        captchaEdit = (EditText)root.findViewById(R.id.captcha_edit);
        captchaEdit.addTextChangedListener(mTextWatcher);
        captchaImg = (ImageView)root.findViewById(R.id.captcha_img);
        captchaImg.setOnClickListener(view->{
            mPresenter.getCaptcha();
        });
        mProgressBar = (ProgressBar)root.findViewById(R.id.register_progress);
        return root;
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(mailEdit.getText().toString().trim().length()!=0
                    && passEdit.getText().toString().trim().length()!=0
                    && nameEdit.getText().toString().trim().length()!=0
                    && captchaEdit.getText().toString().trim().length()!=0){
                registerBtn.setEnabled(true);
            }else {
                registerBtn.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new LoginPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull LoginPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean onBackPressed(){
        ActivityUtils.switchBetweenFragment(getActivity().getSupportFragmentManager(),
                getActivity().getSupportFragmentManager().findFragmentByTag(REGISTER_FRAG),
                getActivity().getSupportFragmentManager().findFragmentByTag(LOGIN_FRAG),ActivityUtils.BACKWARD);
        return true;
    }

    private boolean isInputValid(){
        boolean mailResult;
        boolean passResult;
        boolean captchaResult;
        String passPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$";
        String captchaPattern = "^[a-zA-Z\\d]{6}$";
        Animation animation = AnimationUtils.loadAnimation(getActivity(),R.anim.edittext_shake_animation);
        mailResult = android.util.Patterns.EMAIL_ADDRESS.matcher(mailEdit.getText().toString().trim()).matches();
        passResult = Pattern.matches(passPattern,passEdit.getText().toString().trim());
        captchaResult = Pattern.matches(captchaPattern,captchaEdit.getText().toString().trim());
        if(!mailResult){
            mailEdit.startAnimation(animation);
        }
        if(!passResult){
            passEdit.startAnimation(animation);
        }
        if(!captchaResult){
            captchaEdit.startAnimation(animation);
        }

        return mailResult && passResult && captchaResult;
    }

    @Override
    public void setCaptcha(Bitmap bm){
        captchaImg.setImageBitmap(bm);
    }

    @Override
    public void showRegisterResult(int resultCode){
        mProgressBar.setVisibility(View.GONE);
        String resultMsg;
        switch (resultCode){
            case 0:
                resultMsg = getActivity().getString(R.string.register_success);
                break;
            case 1:
                resultMsg = getActivity().getString(R.string.captcha_timeout);
                break;
            case 2:
                resultMsg = getActivity().getString(R.string.captcha_wrong);
                break;
            case 3:
                resultMsg = getActivity().getString(R.string.mail_used_already);
                break;
            case 500:
                resultMsg = getActivity().getString(R.string.register_fail);
                break;
            default:
                resultMsg = getActivity().getString(R.string.register_success);
        }
        Utils.showToast(getActivity(),resultMsg,false);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if(!hasLoaded){
                mPresenter.getCaptcha();
                hasLoaded = true;
            }
        }
    }
}