package com.zkliu.android.yourstock.mvp.main.news;

import com.zkliu.android.yourstock.app.StockApplication;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.main.DaggerMainComponent;

import javax.inject.Inject;

/**
 * Created by liuzekai on 22/01/2017.
 */

public class NewsPresenterFactory implements BasePresenterFactory<NewsPresenter> {

    private final NewsFragment newsFragment;
    public NewsPresenterFactory(NewsFragment mNewsFragment){
        newsFragment = mNewsFragment;
    }

    @Inject
    NewsPresenter newsPresenter;

    @Override
    public  NewsPresenter create(){
        DaggerMainComponent.builder()
                .newsModule(
                        new NewsModule(newsFragment))
                .newsRepositoryComponent(
                        ((StockApplication)newsFragment.getActivity().getApplication()).getNewsRepositoryComponent()
                )
                .build().inject(this);

        return newsPresenter;
    }
}
