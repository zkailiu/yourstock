package com.zkliu.android.yourstock.mvp.photodetail.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.zkliu.android.yourstock.mvp.photodetail.sub.PhotoDetailSubFragment;

import java.util.List;

/**
 * Created by liuzekai on 30/06/2017.
 */

public class PhotoViewAdapter extends FragmentStatePagerAdapter {

    private List<PhotoDetailSubFragment> mFragList;

    public PhotoViewAdapter(FragmentManager fm, List<PhotoDetailSubFragment> list){
        super(fm);
        mFragList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragList.get(position);
    }

    @Override
    public int getCount() {
        return mFragList.size();
    }

}
