package com.zkliu.android.yourstock.mvp.main.news;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.base.backpress.FragmentBackHandler;
import com.zkliu.android.yourstock.mvp.login.LoginActivity;
import com.zkliu.android.yourstock.mvp.main.news.category.AllCategoryAdapter;
import com.zkliu.android.yourstock.mvp.main.news.category.ChoseCategoryAdapter;
import com.zkliu.android.yourstock.mvp.main.news.category.NewsCategoryItemTouchHelperCallback;
import com.zkliu.android.yourstock.mvp.main.news.category.NewsCategorySpaceDecoration;
import com.zkliu.android.yourstock.mvp.main.news.adapters.NewsSubAdapter;
import com.zkliu.android.yourstock.mvp.main.news.sub.NewsSubFragment;
import com.zkliu.android.yourstock.widget.ClickImageView;

import java.util.LinkedList;
import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.NEWS_724;
import static com.zkliu.android.yourstock.util.Constants.NEWS_FINANCE;
import static com.zkliu.android.yourstock.util.Constants.NEWS_HK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_STOCK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_US;

/**
 * Created by liuzekai on 2017/1/2.
 */

public class NewsFragment extends BaseFragment<NewsPresenter, NewsContract.View> implements NewsContract.View, AllCategoryAdapter.onCategoryAllListener,ChoseCategoryAdapter.onCategoryChoseListener,FragmentBackHandler {

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;


    private TabLayout mTabLayout;
    private ClickImageView mNewsCategoryManagerOpen;
    private ViewPager mViewPager;
    private NewsSubAdapter newsSubAdapter;

    //category manager
    private LinearLayout categoryManager;
    private LinearLayout mTabLayoutManager;
    private TabLayout mManagerTitle;
    private ClickImageView mNewsCategoryManagerClose;
    private RecyclerView mChoseRecyc;
    private TextView mAllCategoryTitle;
    private RecyclerView mAllRecyc;
    private ChoseCategoryAdapter mChoseAdapter;
    private AllCategoryAdapter mAllAdapter;
    private List<Integer> mChoseList;
    private List<Integer> mAllList;
    private NewsCategoryItemTouchHelperCallback ncCallback;
    private ItemTouchHelper mItemTouchHelper;
    private boolean isCategoryClickable;
    private boolean isShownCategoryManager;
    private LinkedList<Integer> allList;


    private int newsSubFragHeight;
    private NewsContract.Presenter mPresenter;
    public NewsFragment(){}

    public static NewsFragment newInstance(){
        return new NewsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.news_main_frag,container,false);
        mToolbar = (Toolbar)root.findViewById(R.id.toolbar_main_news);
        TextView toolbarTitle = (TextView)root.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(R.string.bottom_nav_news);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ActionBar ab = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (ab !=null){
            ab.setDisplayShowTitleEnabled(false);
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        setHasOptionsMenu(true);
        mDrawerLayout = (DrawerLayout)root.findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView)root.findViewById(R.id.nav_view);
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }
        if(null != mNavigationView.getHeaderView(0)){
            setupDrawerHeader(mNavigationView.getHeaderView(0));
        }

        mTabLayout = (TabLayout)root.findViewById(R.id.tabLayout_news);
        mViewPager = (ViewPager)root.findViewById(R.id.viewPager_news);
        mNewsCategoryManagerOpen = (ClickImageView)root.findViewById(R.id.manage_category_news_open);
        mNewsCategoryManagerOpen.setOnClickListener(view-> {
            showCategoryManager(mPresenter.getCategoryFromLocal());
            categoryManager.setVisibility(View.VISIBLE);
            ObjectAnimator.ofFloat(categoryManager,"translationY",-newsSubFragHeight,0).setDuration(500).start();
            mTabLayoutManager.setVisibility(View.VISIBLE);
        });

        initNewsCategoryManager(root);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        newsSubAdapter = new NewsSubAdapter(getChildFragmentManager());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.main_frag_menu_top_right,menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupDrawerContent(NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action1:
                        Log.i("action","1111");
                        break;
                    case R.id.action2:
                        Log.i("action","2222");
                        break;
                    case R.id.setting:
                        Log.i("action","setting");
                        break;
                    default:
                        break;
                }
                //item.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void setupDrawerHeader(View view){
        ImageView avatar = (ImageView)view.findViewById(R.id.avatar);
        avatar.setOnClickListener(v->{
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        });
    }

    private void initNewsTabs(List<Integer> list){
        for(int category : list){
            newsSubAdapter.addFragment(NewsSubFragment.newInstance(category));
        }
        mViewPager.setAdapter(newsSubAdapter);
        mViewPager.setOffscreenPageLimit(newsSubAdapter.getCount()-1);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.post(new Runnable() {
            @Override
            public void run() {
                newsSubFragHeight=mViewPager.getHeight()+ mTabLayoutManager.getHeight();
                ObjectAnimator.ofFloat(categoryManager,"translationY",0,-newsSubFragHeight).setDuration(100).start();
            }
        });
    }

    private void reObtainCategory(List<Integer> list){
        newsSubAdapter.setNewOrderList(list);
        boolean isAddNewItem = false;
        for(int category :list){
            if(!newsSubAdapter.getFragmentCategoryList().contains(category)){
                newsSubAdapter.addFragment(NewsSubFragment.newInstance(category));
                isAddNewItem = true;
            }
        }
        if(isAddNewItem){
            newsSubAdapter.notifyDataSetChanged();
            mViewPager.setOffscreenPageLimit(newsSubAdapter.getCount()-1);
        }
        for(int index = 0; index<newsSubAdapter.getFragmentCategoryList().size(); index++){
            if(!list.contains(newsSubAdapter.getFragmentCategoryList().get(index)))
                newsSubAdapter.removeFragment(index);
        }
        newsSubAdapter.notifyDataSetChanged();
    }

    private void initNewsCategoryManager(View root){
        categoryManager = (LinearLayout)root.findViewById(R.id.category_manager);
        mTabLayoutManager = (LinearLayout)root.findViewById(R.id.tab_layout_manager);
        mManagerTitle = (TabLayout)root.findViewById(R.id.category_manager_title);
        mManagerTitle.addTab(mManagerTitle.newTab().setText(R.string.category_manager_title));
        mNewsCategoryManagerClose = (ClickImageView)root.findViewById(R.id.manage_category_news_close);
        mNewsCategoryManagerClose.setOnClickListener(view->{
            closeCategoryManager();
        });
        mChoseRecyc = (RecyclerView)categoryManager.findViewById(R.id.category_chose);
        mChoseRecyc.setLayoutManager(new GridLayoutManager(getActivity(),3));
        mChoseRecyc.addItemDecoration(new NewsCategorySpaceDecoration(15));

        mAllRecyc = (RecyclerView)categoryManager.findViewById(R.id.category_all);
        mAllRecyc.setLayoutManager(new GridLayoutManager(getActivity(),3));
        mAllRecyc.addItemDecoration(new NewsCategorySpaceDecoration(15));
        mAllCategoryTitle = (TextView)root.findViewById(R.id.all_category_all_title);

        mChoseList = new LinkedList<>();
        mAllList = new LinkedList<>();
        mChoseAdapter = new ChoseCategoryAdapter(getActivity(), mChoseList);
        mChoseAdapter.setListener(this);
        mAllAdapter = new AllCategoryAdapter(getActivity(), mAllList);
        mAllAdapter.setListener(this);

        mChoseRecyc.setAdapter(mChoseAdapter);
        mAllRecyc.setAdapter(mAllAdapter);
        ncCallback = new NewsCategoryItemTouchHelperCallback(mChoseAdapter);
        mItemTouchHelper = new ItemTouchHelper(ncCallback);
        mItemTouchHelper.attachToRecyclerView(mChoseRecyc);

        allList = new LinkedList<>();
        allList.add(NEWS_FINANCE);
        allList.add(NEWS_STOCK);
        allList.add(NEWS_US);
        allList.add(NEWS_HK);
        allList.add(NEWS_724);
    }

    private void showCategoryManager(List<Integer> list){
        mChoseList.clear();
        mChoseList.addAll(list);
        mChoseAdapter.notifyDataSetChanged();

        mAllList.clear();
        for(int category : allList){
            if(!list.contains(category))
                mAllList.add(category);
        }
        mAllAdapter.notifyDataSetChanged();
        isCategoryClickable = true;
        isShownCategoryManager = true;
    }

    @Override
    public void CategoryAllItemClick(final View view, final int position){
        if(isCategoryClickable){
            choseOrNot(view, position, mAllRecyc, mChoseRecyc, mAllList, mChoseList, mAllAdapter, mChoseAdapter);
        }
    }

    @Override
    public void CategoryChoseItemClick(View view, int position){
        if(isCategoryClickable && position !=0){
            choseOrNot(view, position, mChoseRecyc, mAllRecyc, mChoseList, mAllList, mChoseAdapter, mAllAdapter);
        }
    }

    private void choseOrNot(View view, int position, RecyclerView sourceRecyc, RecyclerView targetRecyc,
                            List<Integer> sourceList, List<Integer> targetList,
                            RecyclerView.Adapter sourceAdapter, RecyclerView.Adapter targetAdapter){
        isCategoryClickable = false;
        sourceRecyc.setItemAnimator(new DefaultItemAnimator());
        targetRecyc.setItemAnimator(new DefaultItemAnimator());
        targetList.add(targetList.size(), sourceList.get(position));
        sourceList.remove(position);
        sourceAdapter.notifyDataSetChanged();
        targetAdapter.notifyDataSetChanged();
        sourceAdapter.notifyItemRemoved(position);
        targetAdapter.notifyItemInserted(targetList.size());
        isCategoryClickable = true;
//cancel the animation
//        final PathMeasure mPathMeasure;
//        final float[] mCurrentPosition = new float[2];
//        int parentLoc[] = new int[2];
//        categoryManager.getLocationInWindow(parentLoc);
//
//        int startLoc[] = new int[2];
//        view.getLocationInWindow(startLoc);
//
//        final View startView = view;
//        startView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//        sourceRecyc.removeView(view);
//        categoryManager.addView(startView);
//
//        final View endView;
//        float toX, toY;
//        int endLoc[] = new int[2];
//        int targetCount = targetList.size();
//
//        if(targetCount==0){
//            targetRecyc.getLocationInWindow(endLoc);
//            toX = endLoc[0];
//            int tempLoc[] = new int[2];
//            mAllCategoryTitle.getLocationInWindow(tempLoc);
//            toY = tempLoc[1] - parentLoc[1] + view.getHeight();
//        }else if (targetCount % 3 ==0){
//            endView = targetRecyc.getChildAt(targetCount - 3);
//            endView.getLocationInWindow(endLoc);
//            toX = endLoc[0] - parentLoc[0];
//            toY = endLoc[1] - parentLoc[1] + view.getHeight();
//        }else {
//            endView = targetRecyc.getChildAt(targetCount - 1);
//            endView.getLocationInWindow(endLoc);
//            toX = endLoc[0] - parentLoc[0] + view.getWidth();
//            toY = endLoc[1] - parentLoc[1];
//        }
//
//        float startX = startLoc[0] - parentLoc[0];
//        float startY = startLoc[1] - parentLoc[1];
//
//        Path path = new Path();
//        path.moveTo(startX, startY);
//        path.lineTo(toX, toY);
//        mPathMeasure = new PathMeasure(path, false);
//
//        ViewGroup.LayoutParams fixedParams = startView.getLayoutParams();
//        fixedParams.width = view.getWidth();
//
//        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, mPathMeasure.getLength());
//        if(endLoc[0] !=0 && endLoc[1] != 0){
//            valueAnimator.setDuration(200);
//        }else {
//            Log.i("o","o");
//            valueAnimator.setDuration(0);
//        }
//        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
//        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                float value = (Float) animation.getAnimatedValue();
//                mPathMeasure.getPosTan(value, mCurrentPosition, null);
//                startView.setX(mCurrentPosition[0]);
//                startView.setY(mCurrentPosition[1]);
//                startView.setLayoutParams(fixedParams);
//            }
//        });
//        valueAnimator.start();
//
//        valueAnimator.addListener(new AnimatorListenerAdapter(){
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                sourceRecyc.setItemAnimator(new DefaultItemAnimator());
//                targetRecyc.setItemAnimator(new DefaultItemAnimator());
//                targetList.add(targetList.size(), sourceList.get(position));
//                sourceList.remove(position);
//                sourceAdapter.notifyDataSetChanged();
//                targetAdapter.notifyDataSetChanged();
//                sourceAdapter.notifyItemRemoved(position);
//                targetAdapter.notifyItemInserted(targetList.size());
//                categoryManager.removeView(startView);
//                isCategoryClickable = true;
//            }
//        });
    }

    private void closeCategoryManager(){
        mPresenter.saveCategoryToLocal(mChoseAdapter.getList());
        isShownCategoryManager = false;
        ObjectAnimator close = ObjectAnimator.ofFloat(categoryManager,"translationY",0,-newsSubFragHeight);
        close.addListener(new AnimatorListenerAdapter(){
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                categoryManager.setVisibility(View.GONE);
                mTabLayoutManager.setVisibility(View.GONE);
            }
        });
        close.setDuration(500).start();
        reObtainCategory(mChoseAdapter.getList());
    }

    @Override
    public boolean onBackPressed(){
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }
        if(isShownCategoryManager){
            closeCategoryManager();
            return true;
        }else {
            return false;
        }
    }

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new NewsPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull NewsPresenter presenter) {
        this.mPresenter = presenter;
        initNewsTabs(mPresenter.getCategoryFromLocal());
    }
}
