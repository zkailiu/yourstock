package com.zkliu.android.yourstock.mvp.login;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.zkliu.android.yourstock.data.source.users.UsersRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;


/**
 * Created by liuzekai on 06/07/2017.
 */

public class LoginPresenter implements LoginContract.Presenter {

    @NonNull
    private LoginContract.View mLoginView;

    @NonNull
    private final UsersRepository mUsersRepository;

    @NonNull
    private final CompositeDisposable compositeDisposable;

    @Inject
    LoginPresenter(LoginContract.View loginView, UsersRepository usersRepository){
        mLoginView = loginView;
        mUsersRepository = usersRepository;
        compositeDisposable = new CompositeDisposable();
    }
    @Override
    public void onViewAttached(LoginContract.View view){
        this.mLoginView = view;
    }

    @Override
    public void onViewDetached() {
        mLoginView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }

    @Override
    public void getCaptcha(){
        mUsersRepository.getCaptcha()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Bitmap>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(Bitmap bm){
                        mLoginView.setCaptcha(bm);
                    }
                    @Override
                    public void onError(Throwable e){
                        Log.e("getCaptcha",e.getMessage());
                    }
                    @Override
                    public void onComplete(){

                    }
                });
    }

    @Override
    public void register(String mail, String password, String captcha, String name){
        mUsersRepository.register(mail, password, captcha, name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Integer>() {
                    @Override
                    public void onNext(Integer resultCode) {
                        switch (resultCode){
                            case 0:
                                Log.i("isSuccess","yes");
                                break;
                            case 1:
                            case 2:
                            case 3:
                                Log.i("isSuccess","no");
                                break;
                        }
                        mLoginView.showRegisterResult(resultCode);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("getCaptcha",e.getMessage());
                        mLoginView.showRegisterResult(500);
                    }

                    @Override
                    public void onComplete() {

                    }
                }) ;
    }

    public void login(String mail, String password){

    }

}
