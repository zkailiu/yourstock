package com.zkliu.android.yourstock.retrofit;

import com.zkliu.android.yourstock.retrofit.gson.HeadLineNewsGson;
import com.zkliu.android.yourstock.retrofit.gson.NewsDetailGson;
import com.zkliu.android.yourstock.retrofit.gson.NonHeadLineNewsGson;
import com.zkliu.android.yourstock.retrofit.gson.RegisterGson;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by liuzekai on 09/04/2017.
 */

public interface RetrofitService {

    @GET(Api.NEWS +"{category}")
    Observable<HeadLineNewsGson> getHeadlineNews(@Path("category") String category);

    @GET(Api.NEWS +"{category}"+"/"+"{idlist}")
    Observable<NonHeadLineNewsGson> getNonHeadlineNews(@Path("category") String category, @Path("idlist") String idlist);

    @GET(Api.NEWS_DETAIL + "{id}")
    Observable<NewsDetailGson> getNewsDetail(@Path("id") String id);

    @GET(Api.USERS + "captcha")
    Observable<Response<ResponseBody>> getCaptcha();

    @FormUrlEncoded
    @POST(Api.USERS)
    Observable<RegisterGson>  register(@Field("mail") String mail,
                                       @Field("password") String password,
                                       @Field("captcha") String captcha,
                                       @Field("name") String name);

}
