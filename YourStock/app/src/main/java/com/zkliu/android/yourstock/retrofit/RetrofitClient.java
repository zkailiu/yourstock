package com.zkliu.android.yourstock.retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.zkliu.android.yourstock.app.StockApplication.getAppContext;

/**
 * Created by liuzekai on 09/04/2017.
 */

public class RetrofitClient {

    private static volatile OkHttpClient sOkHttpClient;

    private RetrofitClient(){

    }

    private static class ClientHolder{
        private static Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.API_BASE)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static Retrofit getInstance(){
        return ClientHolder.retrofit;
    }

    private static OkHttpClient getOkHttpClient(){
        if(sOkHttpClient==null){
            synchronized(RetrofitClient.class){
                if(sOkHttpClient == null){
                    sOkHttpClient = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(20, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)
                            .addInterceptor(new AddCookiesInterceptor())
                            .addInterceptor(new SaveCookiesInterceptor())
                            .build();
                }
            }
        }
        return sOkHttpClient;
    }

    private static class AddCookiesInterceptor implements Interceptor{

        @Override
        public Response intercept(Chain chain) throws IOException {
            if (chain == null)
                Log.d("http", "Addchain == null");
            final Request.Builder builder = chain.request().newBuilder();
            SharedPreferences sharedPreferences = getAppContext().getSharedPreferences("cookie", Context.MODE_PRIVATE);
            Observable.just(sharedPreferences.getString("cookie", ""))
                    .subscribe(new Consumer<String>() {
                        @Override
                        public void accept(@NonNull String cookie) throws Exception {
                            builder.addHeader("cookie", cookie);
                        }
                    });
            return chain.proceed(builder.build());
        }
    }

    private static class SaveCookiesInterceptor implements Interceptor{

        @Override
        public Response intercept(Chain chain) throws IOException {
            if (chain == null)
                Log.d("http", "Receivedchain == null");
            Response originalResponse = chain.proceed(chain.request());
            if (!originalResponse.headers("set-cookie").isEmpty()) {
                final StringBuffer cookieBuffer = new StringBuffer();
                Disposable subscribe = Observable.fromIterable(originalResponse.headers("set-cookie"))
                        .map(new Function<String, String>() {
                            @Override
                            public String apply(@NonNull String list) throws Exception{
                                String[] split = list.split(";");
                                return split[0];
                            }
                        })
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(@NonNull String cookie) throws Exception {
                                if(!TextUtils.isEmpty(cookie)){
                                    cookieBuffer.append(cookie).append(";");
                                }
                            }
                        });
                SharedPreferences.Editor editor = getAppContext().getSharedPreferences("cookie", Context.MODE_PRIVATE).edit();
                editor.putString("cookie", cookieBuffer.toString());
                editor.commit();
                subscribe.dispose();
            }

            return originalResponse;
        }
    }

}
