package com.zkliu.android.yourstock.data.source.users;

import android.graphics.Bitmap;

import io.reactivex.Observable;

/**
 * Created by liuzekai on 12/07/2017.
 */

public interface UsersDataSource {

    Observable<Bitmap> getCaptcha();

    Observable<Integer> register(String mail, String password, String captcha, String name);
}
