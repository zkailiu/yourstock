package com.zkliu.android.yourstock.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import io.reactivex.Observable;


/**
 * Created by liuzekai on 08/04/2017.
 */

public class NetworkUtil {

    private Context mContext;
    private BroadcastReceiver mBroadcastReceiver;

    public NetworkUtil(Context context){
        this.mContext = context;
    }

    //used when open this app
    public boolean isInternetOn(){
        ConnectivityManager connectivityManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activiteNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activiteNetworkInfo != null && activiteNetworkInfo.isConnected();
    }

    public Observable<Boolean> isInternetOnObservable(){
        return Observable.just(this.isInternetOn());
    }

    /* Register for Internet connection change broadcast receiver */
//    public void registerBroadCastReceiver(){
//        final IntentFilter filter = new IntentFilter();
//        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
//
//        mBroadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                Bundle extras = intent.getExtras();
//                NetworkInfo info = extras.getParcelable("networkInfo");
//                assert info != null;
//
//            }
//        }
//
//    }

}
