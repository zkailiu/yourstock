package com.zkliu.android.yourstock.data.source.users;

import com.zkliu.android.yourstock.app.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by zekail on 12/7/2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class, UsersRepositoryModule.class})
public interface UsersRepositoryComponent {

    UsersRepository getUsersRepository();
}
