package com.zkliu.android.yourstock.mvp.photodetail;

import com.zkliu.android.yourstock.data.source.news.NewsRepositoryComponent;
import com.zkliu.android.yourstock.mvp.base.FragmentScoped;

import dagger.Component;

/**
 * Created by zekail on 28/6/2017.
 */
@FragmentScoped
@Component(modules = PhotoDetailModule.class,
        dependencies = NewsRepositoryComponent.class)
public interface PhotoDetailComponent {
    void inject(PhotoDetailresenterFactory photoDetailresenterFactory);
}
