package com.zkliu.android.yourstock.widget.scaleImageView;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnGestureListener {

    void onDrag(float dx, float dy);

    void onFling(float startX, float startY, float velocityX,
                 float velocityY);

    void onScale(float scaleFactor, float focusX, float focusY);
}
