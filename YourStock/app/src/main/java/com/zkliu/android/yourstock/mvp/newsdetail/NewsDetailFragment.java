package com.zkliu.android.yourstock.mvp.newsdetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.base.backpress.FragmentBackHandler;
import com.zkliu.android.yourstock.mvp.photodetail.PhotoDetailActivity;
import com.zkliu.android.yourstock.util.ImageLoader;
import com.zkliu.android.yourstock.widget.MyBottomSheetDialog;
import com.zkliu.android.yourstock.widget.NestedScrollWebView;
import com.zkliu.android.yourstock.widget.sliderbar.FontSliderBar;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_CATEGORY_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_ID_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_IMGURL_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_TITLE_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_NEWSID;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_HEIGHT;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_SCREENLOCATION_LEFT;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_SCREENLOCATION_TOP;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_WIDTH;
import static com.zkliu.android.yourstock.util.Constants.VIEW_INFO_EXTRA_FRAG;


/**
 * Created by zekail on 8/6/2017.
 */
@SuppressLint("JavascriptInterface")
public class NewsDetailFragment extends BaseFragment<NewsDetailPresenter, NewsDetailContract.View> implements NewsDetailContract.View,FragmentBackHandler,
         FontSliderBar.OnViewClickListener,FontSliderBar.OnSliderBarChangeListener{
    private String newsDetailTitle;
    private String newsDetailID;
    private String newsDetailImgurl;
    private String newsDetailCategory;
    private Bundle locationInfoBundle;

    private ImageView detailImg;
    private Toolbar detailToolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private NestedScrollWebView mWebView;
    private ProgressBar loadingProgBar;
    private LinearLayout mNewsBottomLL;
    boolean isBottomShow = true;

    MyBottomSheetDialog mDialog;
    private FontSliderBar sliderBar;
    private int textPadding;
    private int textSize;
    private int thumbRadius;
    private int tickRadius;
    private int currentTextSize;

    private NewsDetailContract.Presenter mPresenter;

    public NewsDetailFragment(){
    }

    public static NewsDetailFragment newInstance(){
        return new NewsDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        newsDetailTitle = getArguments().getString(NEWS_DETAIL_TITLE_TO_FRAG);
        newsDetailID = getArguments().getString(NEWS_DETAIL_ID_TO_FRAG);
        newsDetailImgurl = getArguments().getString(NEWS_DETAIL_IMGURL_TO_FRAG);
        newsDetailCategory = getArguments().getString(NEWS_DETAIL_CATEGORY_TO_FRAG);
        locationInfoBundle = getArguments().getBundle(VIEW_INFO_EXTRA_FRAG);

        View root = inflater.inflate(R.layout.frag_news_detail,container,false);
        collapsingToolbarLayout = (CollapsingToolbarLayout)root.findViewById(R.id.news_detail_collasping_toolbar);
        detailImg = (ImageView)root.findViewById(R.id.news_detail_bar_image);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            ActivityCompat.postponeEnterTransition(getActivity());
            if(newsDetailImgurl!=null && !newsDetailImgurl.isEmpty()){
                ImageLoader.loadWithOutScale(getActivity(),newsDetailImgurl,detailImg,new Callback() {
                    @Override
                    public void onSuccess() {
                        ActivityCompat.startPostponedEnterTransition(getActivity());
                    }

                    @Override
                    public void onError() {
                        //ActivityCompat.startPostponedEnterTransition(getActivity());
                    }
                });
            }else {
                ActivityCompat.startPostponedEnterTransition(getActivity());
            }

        }else {
            detailImg.setVisibility(View.INVISIBLE);
            if(newsDetailImgurl!=null && !newsDetailImgurl.isEmpty()){
                ImageLoader.loadWithOutScale(getActivity(), newsDetailImgurl, detailImg, new Callback() {
                    @Override
                    public void onSuccess() {
                        onUiReady();
                    }

                    @Override
                    public void onError() {
                        //onUiReady();
                    }
                });
            }
            collapsingToolbarLayout.setTitle(newsDetailTitle);
        }


        detailToolbar = (Toolbar)root.findViewById(R.id.news_detail_toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(detailToolbar);
        setHasOptionsMenu(true);

        loadingProgBar = (ProgressBar)root.findViewById(R.id.news_loading_progress_bar);
        loadingProgBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(),R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);

        mWebView = (NestedScrollWebView)root.findViewById(R.id.news_detail_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.addJavascriptInterface(this,"jsFinance");
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setDefaultTextEncodingName("utf-8");
        webSettings.setLoadWithOverviewMode(true);
        mWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("test", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId() );
                return true;
            }
        });
        mWebView.setOnScrollDirectionListener(new NestedScrollWebView.OnScrollDirectionListener() {
            @Override
            public void onScrollDirect(NestedScrollWebView v, int positionNew, int positionOld) {
                if(positionNew - positionOld >0 && isBottomShow){//scroll down
                    isBottomShow = false;
                    mNewsBottomLL.animate().translationY(mNewsBottomLL.getHeight());
                }else if(positionNew - positionOld <0 && !isBottomShow){//scroll up
                    isBottomShow = true;
                    mNewsBottomLL.animate().translationY(0);
                }
            }
        });
        mNewsBottomLL = (LinearLayout)root.findViewById(R.id.news_detail_bottom);

        mDialog = new MyBottomSheetDialog(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_news_detail,null);
        mDialog.setContentView(dialogView);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View)dialogView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_HIDDEN){
                    mDialog.dismiss();
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else if(newState == BottomSheetBehavior.STATE_EXPANDED){
                }else if(newState == BottomSheetBehavior.STATE_COLLAPSED){
                }else if(newState == BottomSheetBehavior.STATE_DRAGGING){
                }else if(newState == BottomSheetBehavior.STATE_SETTLING){
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        textSize = (int) TypedValue.applyDimension(2, 16.0f, getResources().getDisplayMetrics());
        textPadding = (int) TypedValue.applyDimension(1, 24.0f, getResources().getDisplayMetrics());
        thumbRadius = (int) TypedValue.applyDimension(1, 8.0f, getResources().getDisplayMetrics());
        tickRadius = (int) TypedValue.applyDimension(1, 6.0f, getResources().getDisplayMetrics());
        sliderBar = (FontSliderBar)dialogView.findViewById(R.id.news_detail_font_slider);
        sliderBar.setTickCount(4)
                .setTickHeight((float) tickRadius)
                .setBarColor(Color.parseColor("#dee4f4"))
                .setTextColor(Color.parseColor("#333333"))
                .setTextPadding(textPadding)
                .setTextSize(this.textSize)
                .setThumbRadius((float) thumbRadius)
                .setThumbColorNormal(Color.parseColor("#007aff"))
                .setThumbColorPressed(Color.parseColor("#acd6ff"))
                .withAnimation(true);//.apply();
        sliderBar.setOnSliderBarChangeListener(this);
        sliderBar.setOnViewClickListener(this);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Transition transition = getActivity().getWindow().getSharedElementEnterTransition();
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {
                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    collapsingToolbarLayout.setTitle(newsDetailTitle);
                    if(mPresenter != null){
                        mPresenter.getNewsContent(newsDetailID,newsDetailCategory);
                    }
                }

                @Override
                public void onTransitionCancel(Transition transition) {
                }

                @Override
                public void onTransitionPause(Transition transition) {
                }

                @Override
                public void onTransitionResume(Transition transition) {
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new NewsDetailPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull NewsDetailPresenter presenter) {
        this.mPresenter = presenter;
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            mPresenter.getNewsContent(newsDetailID,newsDetailCategory);
        }
    }

    @Override
    public void showContent(String content){
        currentTextSize = mPresenter.getFontSize();
        if(currentTextSize != 1){
            String charSequence = "onload=\"javascript:changeSizeForIndex('" + currentTextSize + "')\"";
            content = content.replace("onload=\"javascript:changeSizeForIndex('1')\"", charSequence);
        }
        mWebView.loadDataWithBaseURL(null,content,"text/html", "utf-8",null);
    }

    @Override
    public void showProgress() {
        loadingProgBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loadingProgBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onBackPressed(){
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_news_font_size, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_change_font_size:
                Log.i("gaizi","gaizi");
                sliderBar.setSelectIndex(currentTextSize)
                        .apply();
                sliderBar.setItemSelected(currentTextSize,false);
                mDialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(int index){
        Log.i("index2",""+index);//call same function
    }
    public void onIndexChanged(FontSliderBar fontSliderBar, int index){
        if(currentTextSize != index){
            currentTextSize = index;
            sliderBar.setItemSelected(index,true);
            mPresenter.saveFontSize(index);
            changeTextFontSize(currentTextSize);
            Log.i("index1",""+index);//call same function
        }
    }

    private void changeTextFontSize(int size){
        if(mWebView!=null){
            mWebView.loadUrl("javascript:changeSizeForIndex('" + size + "');");
        }
    }

    @JavascriptInterface
    public void showImageDetail(String str){
        Intent intent = new Intent(getActivity(), PhotoDetailActivity.class);
        intent.putExtra(PHOTO_DETAIL_URL,str);
        intent.putExtra(PHOTO_DETAIL_URL_NEWSID,newsDetailID);
        startActivity(intent);
    }

    private void onUiReady(){
        detailImg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                // remove previous listener
                detailImg.getViewTreeObserver().removeOnPreDrawListener(this);
                // prep the scene
                prepareScene();
                // run the animation
                runEnterAnimation();
                return true;
            }
        });
    }

    private void prepareScene() {
        int originViewLeft = locationInfoBundle.getInt(PROPNAME_SCREENLOCATION_LEFT);
        int originViewTop = locationInfoBundle.getInt(PROPNAME_SCREENLOCATION_TOP);
        int originViewWidth = locationInfoBundle.getInt(PROPNAME_WIDTH);
        int originViewHeight = locationInfoBundle.getInt(PROPNAME_HEIGHT);

        int[] screenLocation = new int[2];
        detailImg.getLocationOnScreen(screenLocation);
        Log.i("location0",""+screenLocation[0]+" "+screenLocation[1]);

        float deltaX, deltaY;
        deltaX = originViewLeft - screenLocation[0];
        deltaY = originViewTop - screenLocation[1];
        detailImg.setTranslationX(deltaX);
        detailImg.setTranslationY(deltaY);

        int[] screenLocation1 = new int[2];
        detailImg.getLocationOnScreen(screenLocation1);
        Log.i("location1",""+screenLocation1[0]+" "+screenLocation1[1]);

        float scaleX, scaleY;
        scaleX = (float) originViewWidth / detailImg.getWidth();
        scaleY = (float) originViewHeight / detailImg.getHeight();
        detailImg.setPivotX(screenLocation1[0]);
        detailImg.setPivotY(screenLocation1[1]);
        detailImg.setScaleX(scaleX);
        detailImg.setScaleY(scaleY);
        Log.i("location",""+originViewLeft+" "+originViewTop+" "+originViewWidth+" "+originViewHeight
                +" "+deltaX+" "+deltaY+" "+scaleX+" "+scaleY);

        int[] screenLocation2 = new int[2];
        detailImg.getLocationOnScreen(screenLocation2);
        Log.i("location2",""+screenLocation2[0]+" "+screenLocation2[1]);
    }

    private void runEnterAnimation(){
        detailImg.setVisibility(View.VISIBLE);
        // finally, run the animation
        detailImg.animate()
                .setDuration(1000)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .scaleX(1f)
                .scaleY(1f)
                .translationX(0)
                .translationY(0)
                .start();
    }
}

