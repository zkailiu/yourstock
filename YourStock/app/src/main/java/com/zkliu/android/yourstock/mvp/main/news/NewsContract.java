package com.zkliu.android.yourstock.mvp.main.news;

import com.zkliu.android.yourstock.data.entity.news.News;
import com.zkliu.android.yourstock.mvp.base.BasePresenter;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by liuzekai on 2017/1/2.
 */

public interface NewsContract {

    interface View{

    }

    interface Presenter extends BasePresenter<NewsContract.View>{

        void saveCategoryToLocal(List<Integer> list);

        List<Integer> getCategoryFromLocal();
    }
}
