package com.zkliu.android.yourstock.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by liuzekai on 03/07/2017.
 */

public class Utils {

    private static Toast toast;

    public static void showToast(Context context, String content, boolean isLong){
        if(toast == null){
            toast = Toast.makeText(context,content,isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        }else {
            toast.setText(content);
        }
        toast.show();
    }
}
