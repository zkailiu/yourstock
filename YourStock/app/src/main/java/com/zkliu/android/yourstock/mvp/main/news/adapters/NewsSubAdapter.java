package com.zkliu.android.yourstock.mvp.main.news.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.zkliu.android.yourstock.mvp.main.news.sub.NewsSubFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zekail on 4/5/2017.
 */
//http://stackoverflow.com/questions/10849552/update-viewpager-dynamically  http://blog.csdn.net/qq_22706515/article/details/51426770
    //http://blog.csdn.net/bluer1244/article/details/50135729
    //https://android.googlesource.com/platform/development/+/f212598/samples/SupportDesignDemos/src/com/example/android/support/design/widget/TabLayoutUsage.java
    //http://stackoverflow.com/questions/34306476/dynamically-add-and-remove-tabs-in-tablayoutmaterial-design-android
public class NewsSubAdapter extends FragmentPagerAdapter {

    private final List<NewsSubFragment> mFragmentList = new ArrayList<>();
    private final List<NewsSubFragment> mFragmentSavedList = new ArrayList<>();
    private final List<Integer> newOrderList = new ArrayList<>();


    public NewsSubAdapter(FragmentManager fm){
        super(fm);
    }

    public void addFragment(NewsSubFragment fragment){
        boolean isOldCategory = false;
        for(int index = 0; index<mFragmentSavedList.size();index++){
            if(fragment.getCategory()==mFragmentSavedList.get(index).getCategory()){
                isOldCategory = true;
                mFragmentList.add(mFragmentSavedList.get(index));
            }
        }
        if(!isOldCategory){
            mFragmentList.add(fragment);
            mFragmentSavedList.add(fragment);
        }
        Log.i("shashaaddFragment",""+mFragmentList.size());
    }

    public List<Integer> getFragmentCategoryList(){
        List<Integer> list = new ArrayList<>();
        for(NewsSubFragment fragment : mFragmentList){
            list.add(fragment.getCategory());
        }
        return list;
    }

    public void removeFragment(int position){
        Log.i("shasharemoveFragment","remove"+position);
        mFragmentList.remove(position);
    }

    @Override
    public int getItemPosition(Object object){
        int index = mFragmentList.indexOf(object);
        int newIndex = newOrderList.indexOf(((NewsSubFragment)object).getCategory());
        if(index != newIndex){
            Collections.swap(mFragmentList,index,newIndex);
        }
        Log.i("shashagetItemPosition","old "+index+" new "+newIndex+" title "+((NewsSubFragment)object).getTitle());
        if (newIndex >= 0)
            return newIndex;
        else
            return POSITION_NONE;
    }

    public void setNewOrderList(List<Integer> list){
        newOrderList.clear();
        newOrderList.addAll(list);
    }

    @Override
    public NewsSubFragment getItem(int position){
        Log.i("shashagetItem",""+position);
        return mFragmentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        int hashCode = mFragmentList.get(position).hashCode();
        Log.i("shashagetItemId",""+position+" "+hashCode);
        return hashCode;
    }

    @Override
    public int getCount(){
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        Log.i("shashagetPageTitle",""+position+" "+mFragmentList.get(position).getTitle());
        return mFragmentList.get(position).getTitle();
    }
}
