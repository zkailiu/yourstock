package com.zkliu.android.yourstock.mvp.main.news.sub.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.data.entity.news.News;
import com.zkliu.android.yourstock.mvp.newsdetail.NewsDetailActivity;

import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_CATEGORY;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_ID;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_IMGURL;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_TITLE;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_HEIGHT;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_SCREENLOCATION_LEFT;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_SCREENLOCATION_TOP;
import static com.zkliu.android.yourstock.util.Constants.PROPNAME_WIDTH;
import static com.zkliu.android.yourstock.util.Constants.VIEW_INFO_EXTRA;

/**
 * Created by liuzekai on 02/04/2017.
 */

public class NewsRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int TYPE_NEWS_REGULAR = 0;
    private static final int TYPE_NEWS_724 = 1;

    private List<News> newsList;
    private Context context;


    public NewsRVAdapter(List<News> list, Context mContext){
        this.newsList = list;
        this.context = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if(!newsList.get(position).getTime().equals("")){
            return TYPE_NEWS_724;
        }else{
            return TYPE_NEWS_REGULAR;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        View v;
        RecyclerView.ViewHolder viewHolder;
        switch (viewType){
            case TYPE_NEWS_REGULAR:
            default:
                v = LayoutInflater.from(context).inflate(R.layout.news_card_item,viewGroup,false);
                viewHolder = new NewsViewHolder(v);
                break;
            case TYPE_NEWS_724:
                v = LayoutInflater.from(context).inflate(R.layout.news_card_724_item,viewGroup,false);
                viewHolder = new News724ViewHolder(v);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i){
        switch (holder.getItemViewType()){
            case TYPE_NEWS_REGULAR:
                NewsViewHolder viewHolder = (NewsViewHolder)holder;
                viewHolder.setTitle(newsList.get(i).getTitle());
                viewHolder.setNewsAbstract(newsList.get(i).getShortNews());
                if(newsList.get(i).getImgUrl().isEmpty()){
                    viewHolder.setImageViewGone();
                }else {
                    viewHolder.setmNewsListPicassoTarget(newsList.get(i).getImgURI());
                }
                viewHolder.itemView.setOnClickListener(view->{
                    Intent intent = new Intent();
                    intent.setClass(this.context, NewsDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(NEWS_DETAIL_TITLE,newsList.get(i).getTitle());
                    bundle.putString(NEWS_DETAIL_ID,newsList.get(i).getId());
                    bundle.putString(NEWS_DETAIL_IMGURL,newsList.get(i).getImgUrl());
                    bundle.putString(NEWS_DETAIL_CATEGORY,newsList.get(i).getCategory());
                    intent.putExtras(bundle);
                    Activity activity = (Activity)this.context;
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//                        Activity activity = (Activity)this.context;
                        this.context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity,
                                viewHolder.mImageView,
                                this.context.getString(R.string.news_detail_transition_photo)).toBundle());
                    }else{
                        intent.putExtra(VIEW_INFO_EXTRA, captureValues(viewHolder.mImageView));
                        this.context.startActivity(intent);
                        activity.overridePendingTransition(0,0);
                    }
                });
                break;
            case TYPE_NEWS_724:
                News724ViewHolder viewHolder724 = (News724ViewHolder)holder;
                viewHolder724.setTime(newsList.get(i).getTime());
                viewHolder724.setContent(newsList.get(i).getContent());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    private Bundle captureValues(@NonNull View view) {
        Bundle b = new Bundle();
        int[] screenLocation = new int[2];
        view.getLocationOnScreen(screenLocation);
        b.putInt(PROPNAME_SCREENLOCATION_LEFT, screenLocation[0]);
        b.putInt(PROPNAME_SCREENLOCATION_TOP, screenLocation[1]);
        b.putInt(PROPNAME_WIDTH, view.getWidth());
        b.putInt(PROPNAME_HEIGHT, view.getHeight());
        return b;
    }


}
