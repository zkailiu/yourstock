package com.zkliu.android.yourstock.widget.scaleImageView;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/**
 * Created by liuzekai on 01/07/2017.
 */

class CustomGestureDetector {

    private static final int INVALID_POINTER_ID = -1;

    private int mActivePointerId = INVALID_POINTER_ID;
    private int mActivePointerIndex = 0;
    private final ScaleGestureDetector mDetector;

    private VelocityTracker mVelocityTracker;
    private boolean mIsDragging;
    private float mLastTouchX;
    private float mLastTouchY;
    private final float mTouchSlop;
    private final float mMinimumVelocity;
    private OnGestureListener mListener;

    CustomGestureDetector(Context context, OnGestureListener listener){
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mTouchSlop = configuration.getScaledTouchSlop();

        mListener = listener;
        ScaleGestureDetector.OnScaleGestureListener mScaleListener = new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                float scaleFactor = detector.getScaleFactor();
                if(Float.isNaN(scaleFactor) || Float.isInfinite(scaleFactor))
                    return false;
                mListener.onScale(scaleFactor,detector.getFocusX(),detector.getFocusY());
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {

            }
        };
        mDetector = new ScaleGestureDetector(context,mScaleListener);
    }

    private float getActiveX(MotionEvent event){
        try {
            return event.getX(mActivePointerIndex);
        }catch (Exception e){
            return event.getX();
        }
    }

    private float getActiveY(MotionEvent event){
        try {
            return event.getY(mActivePointerIndex);
        }catch (Exception e){
            return event.getY();
        }
    }

    public boolean isScaling(){
        return mDetector.isInProgress();
    }

    public boolean isDragging(){
        return mIsDragging;
    }

    public boolean onTouchEvent(MotionEvent event){
        try {
            mDetector.onTouchEvent(event);
            return processTouchEvent(event);
        }catch (IllegalArgumentException e){
            return true;
        }
    }

    private boolean processTouchEvent(MotionEvent event){
        final int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                Log.i("evevtcgd","down");
                mActivePointerId = event.getPointerId(0);

                mVelocityTracker = VelocityTracker.obtain();
                if(mVelocityTracker != null){
                    mVelocityTracker.addMovement(event);
                }

                mLastTouchX = getActiveX(event);
                mLastTouchY = getActiveY(event);
                mIsDragging = false;
                break;
            case MotionEvent.ACTION_MOVE:
                Log.i("evevtcgd","move");
                final float x = getActiveX(event);
                final float y = getActiveY(event);
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                if(!mIsDragging){
                    mIsDragging = Math.sqrt((dx * dx) + (dy * dy)) >= mTouchSlop;
                }

                if(mIsDragging){
                    mListener.onDrag(dx, dy);
                    mLastTouchX = x;
                    mLastTouchY = y;

                    if(mVelocityTracker != null){
                        mVelocityTracker.addMovement(event);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.i("evevtcgd","cancel");
                mActivePointerId = INVALID_POINTER_ID;
                if(mVelocityTracker != null){
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.i("evevtcgd","up");
                mActivePointerId = INVALID_POINTER_ID;
                if(mIsDragging){
                    if(mVelocityTracker != null){
                        mLastTouchX = getActiveX(event);
                        mLastTouchY = getActiveY(event);

                        mVelocityTracker.addMovement(event);
                        mVelocityTracker.computeCurrentVelocity(1000);

                        final float vX = mVelocityTracker.getXVelocity();
                        final float vY = mVelocityTracker.getYVelocity();

                        if(Math.max(Math.abs(vX),Math.abs(vY)) >= mMinimumVelocity){
                            mListener.onFling(mLastTouchX, mLastTouchY, -vX, -vY);
                        }
                    }
                }
                if(mVelocityTracker != null){
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:
                Log.i("evevtcgd","point up");
                final int pointerIndex = ImgUtil.getPointerIndex(event.getAction());
                final int pointerId = event.getPointerId(pointerIndex);
                if(pointerId == mActivePointerId){
                    final int newPointerIndex = pointerIndex == 0?1:0;
                    mActivePointerId = event.getPointerId(newPointerIndex);
                    mLastTouchX = event.getX(newPointerIndex);
                    mLastTouchY = event.getY(newPointerIndex);
                }
                break;
        }

        mActivePointerIndex = event.findPointerIndex(mActivePointerId != INVALID_POINTER_ID ? mActivePointerId : 0);
        return true;
    }

}
