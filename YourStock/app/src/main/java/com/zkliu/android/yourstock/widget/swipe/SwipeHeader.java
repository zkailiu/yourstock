package com.zkliu.android.yourstock.widget.swipe;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.util.DateTimeUtils;

import java.util.Calendar;

/**
 * Created by liuzekai on 14/04/2017.
 */

public class SwipeHeader extends FrameLayout implements CustomSwipeLayout.SwipeTrigger, CustomSwipeLayout.SwipeRefreshTrigger {

    private ImageView ivArrow;

    private ImageView ivSuccess;

    private TextView tvRefresh;

    private ImageView ivProgress;

    private int mHeaderHeight;

    private Animation rotateUp;

    private Animation rotateDown;

    private Animation rotated360;

    private boolean rotated = false;

    private Context mContext;

    public SwipeHeader(Context context) {
        this(context, null);
    }

    public SwipeHeader(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        mHeaderHeight = getResources().getDimensionPixelOffset(R.dimen.swipe_header_height);
        rotateUp = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_up);
        rotateDown = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_down);
        rotated360 = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_360_degree);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        ivArrow = (ImageView)findViewById(R.id.headerArrowIv);
        ivSuccess = (ImageView)findViewById(R.id.headerSuccessIv);
        tvRefresh = (TextView)findViewById(R.id.headerTV);
        ivProgress = (ImageView) findViewById(R.id.headerRefreshIv);
    }

    @Override
    public void onRefresh() {
        ivSuccess.setVisibility(GONE);
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        tvRefresh.setText(R.string.refreshing);
        ivProgress.setVisibility(VISIBLE);
        ivProgress.startAnimation(rotated360);
    }

    @Override
    public void onPrepare() {
    }

    @Override
    public void onMove(int y, boolean isComplete, boolean automatic, boolean hideText) {
        if(!hideText){
            if(!isComplete){
                ivArrow.setVisibility(VISIBLE);
                ivSuccess.setVisibility(GONE);
                ivProgress.clearAnimation();
                ivProgress.setVisibility(GONE);
                if( y > mHeaderHeight){
//use from prestorge
                    tvRefresh.setText(mContext.getString(R.string.release_to_refresh,DateTimeUtils.swipeHeaderFormat.get().format(Calendar.getInstance().getTime())));
                    if(!rotated){
                        ivArrow.clearAnimation();
                        ivArrow.startAnimation(rotateUp);
                        rotated = true;
                    }
                }else if( y < mHeaderHeight){
                    if(rotated){
                        ivArrow.clearAnimation();
                        ivArrow.startAnimation(rotateDown);
                        rotated = false;
                    }
                    tvRefresh.setText(mContext.getString(R.string.swipe_to_refresh,DateTimeUtils.swipeHeaderFormat.get().format(Calendar.getInstance().getTime())));

                }
            }else {
                tvRefresh.setText("RETURNING");
            }
        }
    }

    @Override
    public void onRelease() {
        tvRefresh.setText("onRelease()");
    }

    @Override
    public void onComplete(boolean isRefreshSuccessful) {
        rotated = false;
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        ivSuccess.setVisibility(VISIBLE);
        ivProgress.clearAnimation();
        ivProgress.setVisibility(GONE);
        if(isRefreshSuccessful){
            tvRefresh.setText(mContext.getString(R.string.refresh_complete_success,DateTimeUtils.swipeHeaderFormat.get().format(Calendar.getInstance().getTime())));
        }else {
            tvRefresh.setText(R.string.refresh_complete_fail);
        }
    }

    @Override
    public void onReset() {
        rotated = false;
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        ivProgress.clearAnimation();
        ivProgress.setVisibility(GONE);
        ivSuccess.setVisibility(GONE);
    }
}
