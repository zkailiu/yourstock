package com.zkliu.android.yourstock.data.entity;

/**
 * Created by liuzekai on 02/04/2017.
 */

public interface MapperDTtoV<T> {
    T transform();
}
