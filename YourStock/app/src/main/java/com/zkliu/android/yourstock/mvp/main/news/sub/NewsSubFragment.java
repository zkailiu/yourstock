package com.zkliu.android.yourstock.mvp.main.news.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.data.entity.news.News;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.main.news.sub.adapters.NewsLastItemDecoration;
import com.zkliu.android.yourstock.mvp.main.news.sub.adapters.NewsRVAdapter;
import com.zkliu.android.yourstock.widget.swipe.CustomSwipeLayout;

import java.util.ArrayList;
import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.CATEGORYTEXTMAP;

/**
 * Created by zekail on 4/5/2017.
 */

public class NewsSubFragment extends BaseFragment<NewsSubPresenter, NewsSubContract.View> implements NewsSubContract.View, CustomSwipeLayout.OnRefreshListener, CustomSwipeLayout.OnLoadMoreListener{

    private CustomSwipeLayout mSwipeLayout;
    private RecyclerView news_rv;
    private NewsRVAdapter mNewsRVAdapter;

    private List<News> mNewsList;
    private int mCategory;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasLoaded;

    private NewsSubContract.Presenter mPresenter;


    public static NewsSubFragment newInstance(int category){
        NewsSubFragment subFragment = new NewsSubFragment();
        subFragment.setCategory(category);
        return subFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        mNewsList = new ArrayList<News>();
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.news_sub_frag,container,false);
        mSwipeLayout = (CustomSwipeLayout)root.findViewById(R.id.swipefresh);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setOnLoadMoreListener(this);

        NestedScrollView nestedScrollView = (NestedScrollView)root.findViewById(R.id.news_nested_scrollview);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    mSwipeLayout.setLoadingMore(true);
                }
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setSmoothScrollbarEnabled(true);
        news_rv = (RecyclerView)root.findViewById(R.id.news_RV);
        news_rv.setLayoutManager(llm);
        news_rv.addItemDecoration(new NewsLastItemDecoration(getContext()));
        news_rv.setNestedScrollingEnabled(false);
        mNewsRVAdapter = new NewsRVAdapter(mNewsList,getActivity());
        news_rv.setAdapter(mNewsRVAdapter);
        isPrepared = true;
        return root;
    }

    @Override
    public void onRefresh() {
        mPresenter.refreshHeadLinesNews(mCategory);
    }

    @Override
    public void onLoadMore() {
        mPresenter.getNonHeadLineNews(mCategory);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void showHeadLinesNews(List<News> list){
        mNewsList.clear();
        mNewsList.addAll(list);
        mNewsRVAdapter.notifyDataSetChanged();
        news_rv.invalidateItemDecorations();
    }

    @Override
    public void showNonHeadLineNews(List<News> list){
        mNewsList.addAll(list);
        mNewsRVAdapter.notifyItemInserted(mNewsList.size()-1);
        news_rv.invalidateItemDecorations();
    }

    @Override
    public void startRefreshing(){
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void refreshComplete(boolean isSuccess){
        mSwipeLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(false);
            }
        }, 500);
        mSwipeLayout.setRefreshSuccess(isSuccess);
    }

    @Override
    public void loadMoreComplete(boolean isSuccess){
        mSwipeLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setLoadingMore(false);
            }
        }, 500);
        mSwipeLayout.setLoadMoreSuccess(isSuccess);
    }

    private void setCategory(int category){
        this.mCategory = category;
    }

    public int getCategory(){
        return this.mCategory;
    }

    public String getTitle(){
        return CATEGORYTEXTMAP.get(this.mCategory);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        if(getUserVisibleHint()){
            isVisible = true;
            onVisible();
        }else {
            isVisible = false;
            onInVisible();
        }
    }

    private void onVisible(){
        lazyLoad();
    }

    private void lazyLoad(){
        if(!isVisible || !isPrepared || hasLoaded){
            return;
        }
        hasLoaded = true;
        mPresenter.getHeadLinesNews(mCategory);
    }

    private void onInVisible(){
    }

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new NewsSubPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull NewsSubPresenter presenter) {
        this.mPresenter = presenter;
        lazyLoad();
    }
}
