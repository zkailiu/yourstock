package com.zkliu.android.yourstock.widget.scaleImageView;

import android.widget.ImageView;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnOutsidePhotoTapListener {

    void onOutsidePhotoTap(ImageView imageView);

}
