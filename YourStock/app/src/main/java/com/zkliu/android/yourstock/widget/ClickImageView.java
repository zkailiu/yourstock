package com.zkliu.android.yourstock.widget;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by liuzekai on 07/05/2017.
 */

public class ClickImageView extends ImageView {

    public ClickImageView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
    }

    public ClickImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickImageView(Context context) {
        super(context);
    }

   @Override
   public void setPressed(boolean pressed) {
       updateView(pressed);
       super.setPressed(pressed);
   }

    private void updateView(boolean pressed){
        int brightness=-80;
        ColorMatrix matrix = new ColorMatrix();
        matrix.set(new float[] { 1, 0, 0, 0, brightness, 0, 1, 0, 0,
                brightness, 0, 0, 1, 0, brightness, 0, 0, 0, 1, 0 });
        setColorFilter(new ColorMatrixColorFilter(matrix));
        if( pressed ){
            this.setDrawingCacheEnabled(true);
            this.setColorFilter( new ColorMatrixColorFilter(matrix) ) ;
        }else{
            this.setColorFilter(null) ;
        }
    }
}
