package com.zkliu.android.yourstock.mvp.photodetail;

import com.zkliu.android.yourstock.mvp.base.BasePresenter;

import java.util.List;

/**
 * Created by zekail on 28/6/2017.
 */

public interface PhotoDetailContract {

    interface View{

        void showSaveResult(String result);
    }

    interface Presenter extends BasePresenter<PhotoDetailContract.View> {
        List<String> getImgList(String id);

        void saveImg(final String url);
    }
}
