package com.zkliu.android.yourstock.mvp.main.news.sub;

import com.zkliu.android.yourstock.data.entity.news.News;
import com.zkliu.android.yourstock.mvp.base.BasePresenter;

import java.util.List;

/**
 * Created by zekail on 4/5/2017.
 */

public interface NewsSubContract {

    interface View{
        void showHeadLinesNews(List<News> list);

        void startRefreshing();

        void refreshComplete(boolean isSuccess);

        void showNonHeadLineNews(List<News> list);

        void loadMoreComplete(boolean isSuccess);


    }

    interface Presenter extends BasePresenter<NewsSubContract.View> {
        void getHeadLinesNews(int category);

        void getNonHeadLineNews(int category);

        void refreshHeadLinesNews(int category);
    }
}
