package com.zkliu.android.yourstock.data.source.news;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by liuzekai on 09/04/2017.
 */

public interface NewsDataSource {

    Observable<List<NewsDTO>> getHeadLindNewsList(boolean isRemote, int category);

    void saveHeadLineNewsToDB(List<NewsDTO> newsDTOs);

    Observable<List<NewsDTO>> getNonHeadLineNewsList(int category);

    void saveCategoryToSharedPref(List<Integer> list);

    List<Integer> getCategoryFromSharedPref();

    Observable<String> getNewsDetailContent(String id, String category);

    int getFontSizeFromSharedPref();

    void saveFontSizeToSharedPref(int size);

    List<String> getImgList(String id);

    Observable<String> saveImg(final String url);
}
