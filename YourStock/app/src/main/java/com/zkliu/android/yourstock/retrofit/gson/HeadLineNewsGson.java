package com.zkliu.android.yourstock.retrofit.gson;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;

import java.util.List;

/**
 * Created by zekail on 12/4/2017.
 */

public class HeadLineNewsGson {

    private String nonhead;
    private List<NewsDTO> head;

    public String getNonhead() {
        return nonhead;
    }

    public void setNonhead(String nonhead) {
        this.nonhead = nonhead;
    }

    public List<NewsDTO> getHead() {
        return head;
    }

    public void setHead(List<NewsDTO> head) {
        this.head = head;
    }
}
