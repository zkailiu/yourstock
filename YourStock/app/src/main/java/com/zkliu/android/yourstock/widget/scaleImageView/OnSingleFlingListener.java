package com.zkliu.android.yourstock.widget.scaleImageView;

import android.view.MotionEvent;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnSingleFlingListener {

    boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);

}
