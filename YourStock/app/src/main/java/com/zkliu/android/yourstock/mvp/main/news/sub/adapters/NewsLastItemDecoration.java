package com.zkliu.android.yourstock.mvp.main.news.sub.adapters;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zkliu.android.yourstock.R;

import static com.zkliu.android.yourstock.util.DensityUtil.sp2px;

/**
 * Created by liuzekai on 02/05/2017.
 */

public class NewsLastItemDecoration extends RecyclerView.ItemDecoration {

    Paint mPaint;
    Context mContext;

    public NewsLastItemDecoration(Context context){
        mPaint = new Paint();
        mContext = context;
        mPaint.setColor(0x99EFEFEF);
//        mPaint.setColor(ContextCompat.getColor(mContext, R.color.colorSecondaryText));
//        mPaint.setTextSize(sp2px(mContext,20));
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView parent, RecyclerView.State state){
        final String text = mContext.getString(R.string.swipe_to_load_more);
        final float textFontLength = mPaint.measureText(text);
        final float centerX = (parent.getWidth() - textFontLength)/2;
        final float textFontHeight = mPaint.getFontMetrics().descent - mPaint.getFontMetrics().ascent;
        final float textFontLeading = mPaint.getFontMetrics().leading - mPaint.getFontMetrics().ascent;

        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();
        final int childCount = parent.getChildCount();
        final View child = parent.getChildAt(childCount-1);
        if(child!=null){
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin + Math.round(ViewCompat.getTranslationY(child));
            final int bottom = top + 80;
            canvas.drawRect(left,top,right,bottom,mPaint);

            final float centerY = bottom - 40 - textFontHeight/2 + textFontLeading;
            //canvas.drawText(text,centerX,centerY,mPaint);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
        int itemPosition = parent.getChildAdapterPosition(view);
        int itemSize = parent.getAdapter().getItemCount();
        if(itemPosition == itemSize -1){
            outRect.set(0,0,0,80);
        }
    }
}
