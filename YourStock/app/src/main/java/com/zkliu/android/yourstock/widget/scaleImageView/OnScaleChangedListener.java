package com.zkliu.android.yourstock.widget.scaleImageView;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnScaleChangedListener {

    void onScaleChange(float scaleFactor, float focusX, float focusY);

}
