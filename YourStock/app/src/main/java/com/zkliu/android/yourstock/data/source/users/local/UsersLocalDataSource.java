package com.zkliu.android.yourstock.data.source.users.local;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.zkliu.android.yourstock.data.source.users.UsersDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by zekail on 12/7/2017.
 */
@Singleton
public class UsersLocalDataSource implements UsersDataSource {

    @Inject
    public UsersLocalDataSource(@NonNull Context context){
        checkNotNull(context);
    }

    @Override
    public Observable<Bitmap> getCaptcha(){
        return null;//remote do this
    }

    @Override
    public Observable<Integer> register(String mail, String password, String captcha, String name){
        return null;
    }
}
