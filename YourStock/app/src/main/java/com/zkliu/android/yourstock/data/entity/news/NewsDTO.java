package com.zkliu.android.yourstock.data.entity.news;

import com.zkliu.android.yourstock.data.entity.MapperDTtoV;
import com.zkliu.android.yourstock.util.DateTimeUtils;

import java.util.Calendar;

/**
 * Created by liuzekai on 02/04/2017.
 * this is the Datatrans Obebct, will be used in network
 */

public class NewsDTO implements MapperDTtoV<News> {
    private String newsId;
    private String title;
    private String shortNews;
    private String imgUrl;
    private String category;
    private String content;
    private String time;

    public String getNewsId(){
        return newsId;
    }

    public String getTitle(){
        return title;
    }

    public String getShortNews(){
        return shortNews;
    }

    public String getImgUrl(){
        return imgUrl;
    }

    public String getCategory(){
        return category;
    }

    public String getContent(){
        return content;
    }

    public String getTime(){
        return time;
    }

    public void setNewsId(String mId){
        this.newsId = mId;
    }

    public void setTitle(String mTitle){
        this.title = mTitle;
    }

    public void setShortNews(String mAbstract){
        this.shortNews = mAbstract;
    }

    public void setImgUrl(String mImgUrl){
        this.imgUrl = mImgUrl;
    }

    public void setCategory(String mCategory){
        this.category = mCategory;
    }

    public void setContent(String  mContent){
        this.content = mContent;
    }

    public void setTime(String mTime){
        this.time = mTime;
    }
    @Override
    public News transform(){
        News news = new News();
        news.setId(newsId);
        news.setTitle(title == null ? "":title);
        news.setCategory(category == null ? "":category);
        news.setShortNews(shortNews == null ? "": shortNews);
        news.setImgUrl(imgUrl == null ? "": imgUrl);
        news.setContent(content == null ? "": content);
        news.setTime(time == null ? "": DateTimeUtils.newsDate724Format.get().format(Long.valueOf(time+"000")));
        return news;
    }
}
