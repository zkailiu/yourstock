package com.zkliu.android.yourstock.data.entity.news;


import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by liuzekai on 02/04/2017.
 * this is the news View Object, will be used in view
 */

public class News {

    private String id;
    private String title;
    private String shortNews;
    private String imgUrl;
    private String category;
    private String content;
    private String time;


    public String getId(){
        return id;
    }

    public String getTitle(){
        return title;
    }

    public String getShortNews(){
        return shortNews;
    }

    public String getImgUrl(){
        return imgUrl;
    }

    public URI getImgURI(){
        try{
            return new URI(getImgUrl());
        }catch (URISyntaxException e){
            return null;
        }
    }
    public String getCategory(){
        return category;
    }

    public String getContent(){
        return content;
    }

    public String getTime(){
        return time;
    }

    public void setContent(String  mContent){
        this.content = mContent;
    }

    public void setTime(String mTime){
        this.time = mTime;
    }

    public void setId(String mId){
        this.id = mId;
    }

    public void setTitle(String mTitle){
        this.title = mTitle;
    }

    public void setShortNews(String mShortNews){
        this.shortNews = mShortNews;
    }

    public void setImgUrl(String mImgUrl){
        this.imgUrl = mImgUrl;
    }

    public void setCategory(String mCategory){
        this.category = mCategory;
    }
}
