package com.zkliu.android.yourstock.data.source.news.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by liuzekai on 25/04/2017.
 */

public class NewsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "News.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String COMMA_SEP = ",";

    private static final String UNNIQUE = " UNIQUE";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + NewsPersistenceContract.NewsEntry.TABLE_NAME + " (" +
                    NewsPersistenceContract.NewsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NewsPersistenceContract.NewsEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + UNNIQUE + COMMA_SEP +
                    NewsPersistenceContract.NewsEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    NewsPersistenceContract.NewsEntry.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    NewsPersistenceContract.NewsEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    NewsPersistenceContract.NewsEntry.COLUMN_NAME_IMG_URL + TEXT_TYPE +
                    " )";

    public NewsDbHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }
}
