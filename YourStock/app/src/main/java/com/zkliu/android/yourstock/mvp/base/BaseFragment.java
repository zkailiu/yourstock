package com.zkliu.android.yourstock.mvp.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

/**
 * Created by liuzekai on 2017/1/11.
 */

public abstract class BaseFragment<P extends BasePresenter<V>,V> extends Fragment{

    private static final String TAG = "base-fragment";
    private static final int LOADER_ID = 1;

    private BasePresenter<V> presenter;


    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG,"onActivityCreated");
        // LoaderCallbacks as an object, so no hint regarding loader will be leak to the subclasses.
        getLoaderManager().initLoader(loaderId(), null, new LoaderManager.LoaderCallbacks<P>() {
            @Override
            public final Loader<P> onCreateLoader(int id, Bundle args){
                Log.i(TAG, "onCreateLoader");
                return new PresenterLoader<>(getContext(),getPresenterFactory());
            }

            @Override
            public final void onLoadFinished(Loader<P> loader, P presenter){
                Log.i(TAG, "onLoadFinished");
                BaseFragment.this.presenter = presenter;
                onPresenterPrepared(presenter);
            }

            @Override
            public final void onLoaderReset(Loader<P> loader){
                Log.i(TAG, "onLoaderReset");
                BaseFragment.this.presenter = null;
                onPresenterDestroyed();
            }
        });
    }

    /**
     * Instance of {@link BasePresenterFactory} use to create a Presenter when needed. This instance should
     * not contain {@link android.app.Activity} context reference since it will be keep on rotations.
     */
    @NonNull
    protected abstract BasePresenterFactory<P> getPresenterFactory();

    /**
     * Hook for subclasses that deliver the {@link BasePresenter} before its View is attached.
     * Can be use to initialize the Presenter or simple hold a reference to it.
     */
    protected abstract void onPresenterPrepared(@NonNull P presenter);

    /**
     * Hook for subclasses before the screen gets destroyed.
     */
    protected void onPresenterDestroyed() {
    }

    /**
     * Override in case of fragment not implementing Presenter<View> interface
     */
    @NonNull
    protected V getPresenterView() {
        return (V) this;
    }

    /**
     * Use this method in case you want to specify a spefic ID for the {@link PresenterLoader}.
     * By default its value would be {@link #LOADER_ID}.
     */
    protected int loaderId() {
        return LOADER_ID;
    }
}
