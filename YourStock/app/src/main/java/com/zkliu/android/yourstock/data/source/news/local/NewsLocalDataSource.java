package com.zkliu.android.yourstock.data.source.news.local;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.data.entity.news.NewsDTO;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.zkliu.android.yourstock.data.source.news.local.NewsPersistenceContract.NewsEntry.COLUMN_NAME_CATEGORY;
import static com.zkliu.android.yourstock.data.source.news.local.NewsPersistenceContract.NewsEntry.TABLE_NAME;
import static com.zkliu.android.yourstock.util.Constants.NEWS_724;
import static com.zkliu.android.yourstock.util.Constants.NEWS_FINANCE;
import static com.zkliu.android.yourstock.util.Constants.NEWS_HK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_STOCK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_US;
import static com.zkliu.android.yourstock.util.ImageLoader.getImage;
import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by liuzekai on 09/04/2017.
 */
@Singleton
public class NewsLocalDataSource implements NewsDataSource {

    private NewsDbHelper mDbHelper;
    private SQLiteDatabase db;
    private SharedPreferences sharedPreferences;
    private Context mContext;

    private static final Map<Integer,String> categoryMapLocal;
    static {
        Map<Integer,String> categoryMapInit = new HashMap<Integer, String>();
        categoryMapInit.put(NEWS_FINANCE,"Finance");
        categoryMapInit.put(NEWS_STOCK,"Stock");
        categoryMapInit.put(NEWS_US,"us");
        categoryMapInit.put(NEWS_HK,"hk");
        categoryMapInit.put(NEWS_724,"724");
        categoryMapLocal = Collections.unmodifiableMap(categoryMapInit);
    }

    @Inject
    public NewsLocalDataSource(@NonNull Context context){
        checkNotNull(context);
        mContext = context;
        mDbHelper = new NewsDbHelper(context);
        db = mDbHelper.getReadableDatabase();
        sharedPreferences = context.getSharedPreferences("CATEGORY",Context.MODE_PRIVATE);
    }

    @Override
    public Observable<List<NewsDTO>> getHeadLindNewsList(boolean isRemote, int category){
        String[] projection = {
                NewsPersistenceContract.NewsEntry.COLUMN_NAME_ENTRY_ID,
                NewsPersistenceContract.NewsEntry.COLUMN_NAME_TITLE,
                COLUMN_NAME_CATEGORY,
                NewsPersistenceContract.NewsEntry.COLUMN_NAME_DESCRIPTION,
                NewsPersistenceContract.NewsEntry.COLUMN_NAME_IMG_URL
        };
        String selection = COLUMN_NAME_CATEGORY+"=?";
        String[] selectionArgs = new String[]{categoryMapLocal.get(category)};
        Cursor cursor = db.query(TABLE_NAME, projection,selection,selectionArgs,null,null, NewsPersistenceContract.NewsEntry._ID + " ASC");
        Observable<NewsDTO> observable1 = Observable.create(e -> {
            try{
                if(cursor != null && cursor.getCount() >0){
                    while (cursor.moveToNext()){
                        String newsId = cursor.getString(cursor.getColumnIndexOrThrow(NewsPersistenceContract.NewsEntry.COLUMN_NAME_ENTRY_ID));
                        String newsTitle = cursor.getString(cursor.getColumnIndexOrThrow(NewsPersistenceContract.NewsEntry.COLUMN_NAME_TITLE));
                        String newsCategory = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_CATEGORY));
                        String newsDes = cursor.getString(cursor.getColumnIndexOrThrow(NewsPersistenceContract.NewsEntry.COLUMN_NAME_DESCRIPTION));
                        String newsImg = cursor.getString(cursor.getColumnIndexOrThrow(NewsPersistenceContract.NewsEntry.COLUMN_NAME_IMG_URL));
                        NewsDTO newsDTO = new NewsDTO();
                        newsDTO.setNewsId(newsId);
                        newsDTO.setTitle(newsTitle);
                        newsDTO.setCategory(newsCategory);
                        newsDTO.setShortNews(newsDes);
                        newsDTO.setImgUrl(newsImg);
                        e.onNext(newsDTO);
                    }
                }
            }catch (Exception ex){
                e.onError(ex);
            }finally {
                if(cursor !=null)
                    cursor.close();
                //db.close();
                e.onComplete();
            }
        });
        return observable1.toList().toObservable().subscribeOn(Schedulers.io());
    }

    @Override
    public void saveHeadLineNewsToDB(@NonNull List<NewsDTO> newsDTOs){
        checkNotNull(newsDTOs);
        Log.i("cishu","yici"+newsDTOs.get(0).getCategory());
        db.delete(TABLE_NAME," "+COLUMN_NAME_CATEGORY+ "='"+newsDTOs.get(0).getCategory()+"'",null);

        Observable.fromIterable(newsDTOs)
                .subscribeOn(Schedulers.io())
                .map(newsDTO -> {
                    ContentValues values = new ContentValues();
                    values.put(NewsPersistenceContract.NewsEntry.COLUMN_NAME_ENTRY_ID,newsDTO.getNewsId());
                    values.put(NewsPersistenceContract.NewsEntry.COLUMN_NAME_TITLE,newsDTO.getTitle());
                    values.put(COLUMN_NAME_CATEGORY,newsDTO.getCategory());
                    values.put(NewsPersistenceContract.NewsEntry.COLUMN_NAME_DESCRIPTION,newsDTO.getShortNews());
                    values.put(NewsPersistenceContract.NewsEntry.COLUMN_NAME_IMG_URL,newsDTO.getImgUrl());
                    return db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE) != -1;
                })
                .observeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Boolean>(){
                    @Override
                    public void onNext(Boolean aBoolean){
                        Log.i("savetodb?",aBoolean?"Y":"N");
                    }
                    @Override
                    public void onError(Throwable e){
                        Log.i("savetodb","error");
                    }
                    @Override
                    public void onComplete(){
                        //db.close();
                    }
                });
    }

    @Override
    public Observable<List<NewsDTO>> getNonHeadLineNewsList(int category){
        //do nothing here
        return null;
    };

    @Override
    public void saveCategoryToSharedPref(List<Integer> list){
        String categories = TextUtils.join(",",list);
        Log.i("categorys",categories);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("categorys",categories);
        editor.commit();
    }

    @Override
    public List<Integer> getCategoryFromSharedPref(){
        String categories = sharedPreferences.getString("categorys",null);
        if(categories!=null){
            Integer[] intArray = new Integer[categories.split(",").length];
            int i=0;
            for(String str:categories.split(",")){
                try {
                    intArray[i]=Integer.parseInt(str);
                    i++;
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Not a number: " + str + " at index " + i, e);
                }
            }
            return new LinkedList<Integer>(Arrays.asList(intArray));
        }else {
            List<Integer> defaultCategories = new LinkedList<>();
            defaultCategories.add(NEWS_FINANCE);
            defaultCategories.add(NEWS_STOCK);
            defaultCategories.add(NEWS_US);
            return defaultCategories;
        }
    }

    @Override
    public Observable<String> getNewsDetailContent(String id, String category){
        return Observable.create(e->{
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mContext.getResources().getAssets().open("stock_news.html")));
                StringBuilder stringBuilder = new StringBuilder();
                String readLine;
                while ((readLine = bufferedReader.readLine())!=null){
                    stringBuilder.append(readLine);
                }
                e.onNext(stringBuilder.toString());
                bufferedReader.close();
            }catch (Exception ex){
                e.onError(ex);
            }
        });
    }

    @Override
    public int getFontSizeFromSharedPref(){
        int size = sharedPreferences.getInt("fontSize",1);
        return size;
    }

    @Override
    public void saveFontSizeToSharedPref(int size){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("fontSize",size);
        editor.commit();
    }

    @Override
    public List<String> getImgList(String id){
        return null;//not your job
    }

    @Override
    public Observable<String> saveImg(final String url){
        return Observable.create(e -> {
            Bitmap bitmap = getImage(mContext,url);
            if(bitmap == null){
                e.onError(new IOException(mContext.getString(R.string.image_download_fail)));
            }else {
                String fileName = "/yourStock/photo/" + url.hashCode() + ".jpg";
                File file = new File(Environment.getExternalStorageDirectory(), fileName);
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
                    Intent scannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file));
                    mContext.sendBroadcast(scannerIntent);
                    e.onNext(mContext.getString(R.string.image_save_success_to)+ Uri.fromFile(file).getPath());
                }catch (Exception error){
                    error.printStackTrace();
                    e.onError(new IOException(mContext.getString(R.string.image_save_fail)));
                }finally {
                    if(outputStream != null){
                        outputStream.close();
                    }
                    e.onComplete();
                }
            }
        });
    }

}
