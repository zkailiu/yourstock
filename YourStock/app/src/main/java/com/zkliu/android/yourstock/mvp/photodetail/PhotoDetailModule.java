package com.zkliu.android.yourstock.mvp.photodetail;

import com.zkliu.android.yourstock.mvp.newsdetail.NewsDetailContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by zekail on 28/6/2017.
 */

@Module
public class PhotoDetailModule {

    private final PhotoDetailContract.View mView;

    public PhotoDetailModule(PhotoDetailContract.View view){
        mView = view;
    }

    @Provides
    PhotoDetailContract.View providePhotoDetailContractView(){
        return mView;
    }

}
