package com.zkliu.android.yourstock.data.source.users;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by liuzekai on 12/07/2017.
 */
@Singleton
public class UsersRepository implements UsersDataSource {

    @NonNull
    private final UsersDataSource usersRemoteDataSource;

    @NonNull
    private final UsersDataSource usersLocalDaraSource;

    @Inject
    UsersRepository(@Remote @NonNull UsersDataSource usersRemoteDataSource,
                   @Local @NonNull UsersDataSource usersLocalDaraSource){
        checkNotNull(usersRemoteDataSource);
        checkNotNull(usersLocalDaraSource);
        this.usersRemoteDataSource = usersRemoteDataSource;
        this.usersLocalDaraSource = usersLocalDaraSource;
    }

    @Override
    public Observable<Bitmap> getCaptcha(){
        return usersRemoteDataSource.getCaptcha();
    }

    @Override
    public Observable<Integer> register(String mail, String password, String captcha, String name){
        return usersRemoteDataSource.register(mail,password,captcha,name);
    }

}
