package com.zkliu.android.yourstock.app; /**
 * Created by liuzekai on 2016/12/27.
 */

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * This is a Dagger module. We use this to pass in the Context dependency to the
 * {@link
 *
 */
@Module
public final class ApplicationModule {

    private final Context mContext;

    ApplicationModule(Context context){
        mContext = context;
    }

    @Provides
    Context provideContext(){
        return mContext;
    }
}
