package com.zkliu.android.yourstock.mvp.main.news.sub;

import android.support.annotation.NonNull;
import android.util.Log;

import com.zkliu.android.yourstock.data.entity.news.News;
import com.zkliu.android.yourstock.data.source.news.NewsRepository;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by zekail on 4/5/2017.
 */

public class NewsSubPresenter implements NewsSubContract.Presenter{

    @NonNull
    private final CompositeDisposable compositeDisposable;

    @NonNull
    private NewsSubContract.View mNewsView;

    @NonNull
    private final NewsRepository mNewsRepository;

    @Inject
    NewsSubPresenter(NewsSubContract.View newsView, NewsRepository newsRepository){
        mNewsView = newsView;
        mNewsRepository = newsRepository;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void getHeadLinesNews(int category){
        compositeDisposable.clear();
        Disposable localDisposable;
        localDisposable = mNewsRepository.getHeadLindNewsList(false,category)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .concatMapIterable(list -> list)
                .map(item->item.transform())
                .toList()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<News>>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(List<News> list){
                            mNewsView.showHeadLinesNews(list);
                        }
                    @Override
                    public void onError(Throwable e){
                            Log.e("HLNerrorLocl",e.getMessage());
                        }
                    @Override
                    public void onComplete(){
                            mNewsView.startRefreshing();
                        }
                    });
        compositeDisposable.add(localDisposable);
    }

    @Override
    public void getNonHeadLineNews(int category){
        Disposable disposable = mNewsRepository.getNonHeadLineNewsList(category)
                .observeOn(Schedulers.io())
                .concatMapIterable(list->list)
                .map(item->item.transform())
                .toList()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<News>>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(List<News> list){
                        mNewsView.showNonHeadLineNews(list);
                    }
                    @Override
                    public void onError(Throwable e){
                        Log.e("nonHLNerror",e.getMessage());
                        mNewsView.loadMoreComplete(false);
                    }
                    @Override
                    public void onComplete(){
                        mNewsView.loadMoreComplete(true);
                    }
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void refreshHeadLinesNews(int category){
        Disposable remoteDisposable = mNewsRepository.getHeadLindNewsList(true,category)
                .delay(100, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .concatMapIterable(list -> list)
                .map(item->item.transform())
                .toList()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<News>>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(List<News> list){
                        mNewsView.showHeadLinesNews(list);
                    }
                    @Override
                    public void onError(Throwable e){
                        Log.e("HLNerrorRemote",e.getMessage());
                        mNewsView.refreshComplete(false);
                    }
                    @Override
                    public void onComplete(){
                        mNewsView.refreshComplete(true);
                    }
                });
        compositeDisposable.add(remoteDisposable);
    }

    @Override
    public void onViewAttached(NewsSubContract.View view){
        this.mNewsView = view;
    }

    @Override
    public void onViewDetached() {
        mNewsView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }
}
