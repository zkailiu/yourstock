package com.zkliu.android.yourstock.mvp.main.news.sub.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.main.news.adapters.NewsListPicassoTarget;
import com.zkliu.android.yourstock.util.DensityUtil;
import com.zkliu.android.yourstock.util.ImageLoader;

import java.net.URI;

/**
 * Created by liuzekai on 04/04/2017.
 */

public class NewsViewHolder extends RecyclerView.ViewHolder {

    CardView mCardView;
    ImageView mImageView;
    TextView title;
    TextView newsAbstract;
    NewsListPicassoTarget mNewsListPicassoTarget;
    Context mContext;
    Drawable mDefaultDrawable;

    public NewsViewHolder(final View itemView){
        super(itemView);
        mCardView = (CardView)itemView.findViewById(R.id.news_card);
        title = (TextView)itemView.findViewById(R.id.news_title);
        newsAbstract = (TextView)itemView.findViewById(R.id.news_abstract);
        mImageView = (ImageView)itemView.findViewById(R.id.news_img);
        mContext = itemView.getContext();
        mNewsListPicassoTarget = new NewsListPicassoTarget(mImageView,mContext);
        mDefaultDrawable = mContext.getResources().getDrawable(R.drawable.placeholder300200);
        Bitmap bitmap = ((BitmapDrawable)mDefaultDrawable).getBitmap();
        mDefaultDrawable = new BitmapDrawable(mContext.getResources(),Bitmap.createScaledBitmap(bitmap, DensityUtil.dp2px(mContext,100), DensityUtil.dp2px(mContext,75),true));
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setNewsAbstract(String newsAbstract){
        this.newsAbstract.setText(newsAbstract);
    }

    public void setImageViewGone(){
        this.mImageView.setVisibility(View.GONE);
    }

    public void setmNewsListPicassoTarget(URI uri){
        ImageLoader.loadWithScale(mContext,uri.toString(),mNewsListPicassoTarget,100,80,mDefaultDrawable);
    }

}
