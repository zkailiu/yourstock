package com.zkliu.android.yourstock.mvp.main;

import com.zkliu.android.yourstock.data.source.news.NewsRepositoryComponent;
import com.zkliu.android.yourstock.mvp.main.news.NewsModule;
import com.zkliu.android.yourstock.mvp.main.news.NewsPresenterFactory;
import com.zkliu.android.yourstock.mvp.base.FragmentScoped;

import dagger.Component;

/**
 * Created by liuzekai on 22/01/2017.
 */
@FragmentScoped
@Component(modules = NewsModule.class,
        dependencies = NewsRepositoryComponent.class)
public interface MainComponent {

    void inject(NewsPresenterFactory newsPresenterFactory);

}
