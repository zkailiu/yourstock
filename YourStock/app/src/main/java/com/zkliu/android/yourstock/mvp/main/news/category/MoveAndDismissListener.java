package com.zkliu.android.yourstock.mvp.main.news.category;

/**
 * Created by zekail on 8/5/2017.
 */

public interface MoveAndDismissListener {
    boolean onItemMove(int fromPosition, int toPosition);
}
