package com.zkliu.android.yourstock.mvp.splash;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.zkliu.android.yourstock.util.Constants;
import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.main.MainActivity;
import com.zkliu.android.yourstock.util.DensityUtil;
import com.zkliu.android.yourstock.widget.BezierCircle;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by liuzekai on 2016/12/28.
 * Main UI
 */

public class SplashFragment extends BaseFragment<SplashPresenter,SplashContract.View> implements SplashContract.View {

    private int screenHeight;

    private ImageView bottomLogoImg;
    private BezierCircle bezierCircle;
    private ImageView centerLogoImg;
    private ImageView centerLogoCoverImg;
    private ImageView centerLogoStaticImg;

    private SplashContract.Presenter mPresenter;

    private int CURRENT_ANIMATION_FLAG=10000;

    private AnimatorSet animation0;
    private ObjectAnimator animation1;
    private AnimatorSet animation2;
    final Timer timer3 = new Timer();
    private AnimatorSet animation4;


    public SplashFragment(){
    }

    public static SplashFragment newInstance(){
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.frag_splash,container,false);
        bottomLogoImg = (ImageView)root.findViewById(R.id.bottom_logo);
        bezierCircle = (BezierCircle)root.findViewById(R.id.bezier_circle);
        centerLogoImg = (ImageView)root.findViewById(R.id.center_logo);
        centerLogoCoverImg = (ImageView)root.findViewById(R.id.center_logo_cover);
        centerLogoStaticImg = (ImageView)root.findViewById(R.id.center_logo_static);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            CURRENT_ANIMATION_FLAG = savedInstanceState.getInt("curFlag");
        }
        screenHeight = DensityUtil.getScreenHeight(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        if(CURRENT_ANIMATION_FLAG == 10000){
            mPresenter.start();
        }else {
            centerLogoStaticImg.setVisibility(View.VISIBLE);
            startBottomAnimationReversal();
        }
    }

    @Override
    public void startBottomAnimation(){
        CURRENT_ANIMATION_FLAG = SplashContract.ANIMATIONSTEPZERO;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator alpha = ObjectAnimator.ofFloat(bottomLogoImg, "alpha", 1f, 0f);
                PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 1f, 0f);
                PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 1f, 0f);
                ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(bottomLogoImg,pvhX,pvhY);
                animation0 = new AnimatorSet();
                animation0.play(alpha).with(scale);
                animation0.setDuration(1000);
                animation0.start();
                animation0.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mPresenter.animationFinished(SplashContract.ANIMATIONSTEPONE);
                    }
                });
            }
        },1000);

    }

    @Override
    public void startBezierTransAnimation(){
        CURRENT_ANIMATION_FLAG = SplashContract.ANIMATIONSTEPONE;
        bezierCircle.setVisibility(View.VISIBLE);
        bezierCircle.getLayoutParams().height = DensityUtil.dp2px(getActivity(),4 * Constants.RADIUS);

        int[] location = new int[2];
        bezierCircle.getLocationInWindow(location);
        int yLocation = location[1];
        animation1 = ObjectAnimator.ofFloat(bezierCircle, "translationY", 0,
                (DensityUtil.dp2px(getActivity(), 2 * Constants.RADIUS)+yLocation - screenHeight>>1));
        animation1.setDuration(800);
        animation1.setInterpolator(new AccelerateDecelerateInterpolator());
        animation1.start();
        animation1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float mFraction = (float)(0.5 - Math.abs(animation.getAnimatedFraction() - 0.5));
                bezierCircle.updatePath(0, mFraction* DensityUtil.dp2px(getActivity(), Constants.RADIUS));
            }
        });
        animation1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mPresenter.animationFinished(SplashContract.ANIMATIONSTEPTWO);
            }
        });
    }

    @Override
    public void startBezierRotateAnimation(){
        CURRENT_ANIMATION_FLAG = SplashContract.ANIMATIONSTEPTWO;
        //bezier circle rotate
        ObjectAnimator rotation = ObjectAnimator.ofFloat(bezierCircle, "rotation", 0f, 360f);
        //bezier circle scale
        final float scaleFactor = 2f * centerLogoImg.getHeight() / bezierCircle.getHeight();
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 1f, scaleFactor);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 1f, scaleFactor);
        ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(bezierCircle,pvhX,pvhY);
        //centerlogo and centerlogoCover move up
        int[] location = new int[2];
        bezierCircle.getLocationInWindow(location);
        int bezierCircleY = location[1];
        float targetPosition = bezierCircleY - 0.5f * bezierCircle.getHeight() * (scaleFactor - 1);
        int[] location1 = new int[2];
        centerLogoImg.getLocationInWindow(location1);
        int centerLogoImgY = location1[1];
        ObjectAnimator logoTrans = ObjectAnimator.ofFloat(centerLogoImg,"translationY",0f,
                (targetPosition + 0.25f * bezierCircle.getHeight() *scaleFactor - centerLogoImgY));
        ObjectAnimator logoCoverTrans = ObjectAnimator.ofFloat(centerLogoCoverImg,"translationY",0f,
                (targetPosition + 0.25f * bezierCircle.getHeight() *scaleFactor - centerLogoImgY));

        animation2 = new AnimatorSet();
        animation2.play(rotation).with(scale).with(logoTrans).with(logoCoverTrans);
        animation2.setDuration(500);
        animation2.start();
        rotation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float mFraction = animation.getAnimatedFraction();
                bezierCircle.updatePathrotate(mFraction);
            }
        });
        animation2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                bezierCircle.setVisibility(View.GONE);
                centerLogoImg.setVisibility(View.VISIBLE);
                mPresenter.animationFinished(SplashContract.ANIMATIONSTEPTHREE);
            }
        });
    }

    @Override
    public void startShowCenterLogoCover(){
        CURRENT_ANIMATION_FLAG = SplashContract.ANIMATIONSTEPTHREE;
        centerLogoCoverImg.setVisibility(View.VISIBLE);
        final ClipDrawable drawable = (ClipDrawable)centerLogoCoverImg.getDrawable();
        drawable.setLevel(0);
        final Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == 0x01){
                    drawable.setLevel(drawable.getLevel() + 200);
                }
            }
        };
        timer3.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Message msg = new Message();
                        msg.what = 0x01;
                        mHandler.sendMessage(msg);
                        if(drawable.getLevel() >= 10000){
                            timer3.cancel();
                            mPresenter.animationFinished(SplashContract.ANIMATIONSTEPFOUR);
                        }
                    }
                });
            }
        },0,20);
    }

    @Override
    public void startBottomAnimationReversal(){
        CURRENT_ANIMATION_FLAG = SplashContract.ANIMATIONSTEPFOUR;
        ObjectAnimator alpha = ObjectAnimator.ofFloat(bottomLogoImg, "alpha", 0f, 1f);
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 0f, 1f);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 0f, 1f);
        ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(bottomLogoImg,pvhX,pvhY);
        animation4 = new AnimatorSet();
        animation4.play(alpha).with(scale);
        animation4.setDuration(1000);
        animation4.start();
        animation4.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                },500);
            }
        });
    }

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new SplashPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull SplashPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void cancelCurrentAnimation(){
        switch (CURRENT_ANIMATION_FLAG){
            case SplashContract.ANIMATIONSTEPZERO:
                if(animation0 != null){
                    animation0.removeAllListeners();
                    animation0.cancel();
                    animation0 = null;
                }
                break;
            case SplashContract.ANIMATIONSTEPONE:
                if(animation1 != null){
                    animation1.removeAllListeners();
                    animation1.cancel();
                    animation1 = null;
                    bezierCircle.setVisibility(View.GONE);
                }
                break;
            case SplashContract.ANIMATIONSTEPTWO:
                if(animation2 != null){
                    animation2.removeAllListeners();
                    animation2.cancel();
                    animation2 = null;
                    bezierCircle.setVisibility(View.GONE);
                    centerLogoImg.setVisibility(View.GONE);
                }
                break;
            case SplashContract.ANIMATIONSTEPTHREE:
                timer3.cancel();
                centerLogoImg.setVisibility(View.GONE);
                centerLogoCoverImg.setVisibility(View.GONE);
                break;
            case SplashContract.ANIMATIONSTEPFOUR:
                if(animation4 != null){
                    animation4.removeAllListeners();
                    animation4.cancel();
                    animation4 = null;
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("curFlag", CURRENT_ANIMATION_FLAG);
    }


}
