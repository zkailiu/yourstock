package com.zkliu.android.yourstock.data.source.news.local;

import android.provider.BaseColumns;

/**
 * Created by liuzekai on 25/04/2017.
 */

public class NewsPersistenceContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private NewsPersistenceContract(){}

    /* Inner class that defines the table contents */
    public static abstract class NewsEntry implements BaseColumns{
        public static final String TABLE_NAME = "news";
        public static final String COLUMN_NAME_ENTRY_ID = "newsid";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_IMG_URL = "imgurl";
    }
}
