package com.zkliu.android.yourstock.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import com.zkliu.android.yourstock.R;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by liuzekai on 2016/12/28.
 */

public class ActivityUtils {

    public static final int FORWARD = 1;
    public static final int BACKWARD = 2;

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId, String tag){
        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId,fragment,tag);
        transaction.addToBackStack(tag);
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    public static void switchBetweenFragment(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment sourceFragment,
                                             @NonNull Fragment targetFragment){
        switchBetweenFragment(fragmentManager,sourceFragment,targetFragment,0);
    }

    public static void switchBetweenFragment(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment sourceFragment,
                                             @NonNull Fragment targetFragment,
                                             @Nullable int animationType){
        checkNotNull(fragmentManager);
        checkNotNull(sourceFragment);
        checkNotNull(targetFragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (animationType){
            case FORWARD:
                transaction.setCustomAnimations(R.anim.slide_forward_in,R.anim.slide_forward_out);
                break;
            case BACKWARD:
                transaction.setCustomAnimations(R.anim.slide_backward_in,R.anim.slide_backward_out);
                break;
            default:
                break;
        }
        transaction.hide(sourceFragment);
        transaction.show(targetFragment);
        transaction.commit();
    }
}
