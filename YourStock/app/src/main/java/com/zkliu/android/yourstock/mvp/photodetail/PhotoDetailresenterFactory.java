package com.zkliu.android.yourstock.mvp.photodetail;

import com.zkliu.android.yourstock.app.StockApplication;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;


import javax.inject.Inject;

/**
 * Created by zekail on 28/6/2017.
 */

public class PhotoDetailresenterFactory implements BasePresenterFactory<PhotoDetailPresenter> {

    private final PhotoDetailFragment pDFragment;
    public PhotoDetailresenterFactory(PhotoDetailFragment photoDetailFragment){
        pDFragment = photoDetailFragment;
    }

    @Inject
    PhotoDetailPresenter fdPresenter;

    @Override
    public PhotoDetailPresenter create(){
        DaggerPhotoDetailComponent.builder()
                .photoDetailModule(new PhotoDetailModule(pDFragment))
                .newsRepositoryComponent(((StockApplication)pDFragment.getActivity().getApplication()).getNewsRepositoryComponent())
                .build().inject(this);
        return fdPresenter;
    }
}
