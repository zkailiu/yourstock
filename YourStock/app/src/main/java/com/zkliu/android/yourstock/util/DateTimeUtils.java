package com.zkliu.android.yourstock.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by zekail on 19/4/2017.
 */

public class DateTimeUtils {

    public static final ThreadLocal<SimpleDateFormat> swipeHeaderFormat = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue(){
            return new SimpleDateFormat("MM/dd HH:mm:ss",Locale.CHINA);
        }
    };

    public static final ThreadLocal<SimpleDateFormat> newsDate724Format = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue(){
            return new SimpleDateFormat("MM-dd HH:mm",Locale.CHINA);
        }
    };
}
