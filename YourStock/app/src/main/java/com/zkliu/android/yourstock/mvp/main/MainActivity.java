package com.zkliu.android.yourstock.mvp.main;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;


import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.backpress.BackHandlerHelper;
import com.zkliu.android.yourstock.mvp.main.market.MarketFragment;
import com.zkliu.android.yourstock.mvp.main.news.NewsFragment;
import com.zkliu.android.yourstock.mvp.main.select.SelectFragment;
import com.zkliu.android.yourstock.mvp.main.trade.TradeFragment;
import com.zkliu.android.yourstock.util.ActivityUtils;
import com.zkliu.android.yourstock.util.Utils;
import com.zkliu.android.yourstock.widget.BottomNavigationViewHelper;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private static String CURRENT_FRAGMENT_TAG;
    private final static String NEWS_FRAGMENT_TAG = "NEWSTAG";
    private final static String SELECT_FRAGMENT_TAG = "SELECTTAG";
    private final static String MARKET_FRAGMENT_TAG = "MARKETTAG";
    private final static String TRADE_FRAGMENT_TAG = "TARDETAG";

    private final static String SAVED__TAG_INT = "SAVED_FRAGMENT_INT";
    private int savedTagInt;
    private final static String SAVED__TAG_STR = "SAVED_FRAGMENT_STR";


    private NewsFragment newsFragment;
    private SelectFragment selectFragment;
    private MarketFragment marketFragment;
    private TradeFragment tradeFragment;

    boolean mBackKeyPressed = false;

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);


        if(savedInstanceState == null){
            newsFragment = NewsFragment.newInstance();
            selectFragment = SelectFragment.newInstance();
            marketFragment = MarketFragment.newInstance();
            tradeFragment = TradeFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),newsFragment,R.id.mainFrame,NEWS_FRAGMENT_TAG);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),selectFragment,R.id.mainFrame,SELECT_FRAGMENT_TAG);
            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(NEWS_FRAGMENT_TAG),
                    getSupportFragmentManager().findFragmentByTag(SELECT_FRAGMENT_TAG));
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),marketFragment,R.id.mainFrame,MARKET_FRAGMENT_TAG);
            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(SELECT_FRAGMENT_TAG),
                    getSupportFragmentManager().findFragmentByTag(MARKET_FRAGMENT_TAG));
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),tradeFragment,R.id.mainFrame,TRADE_FRAGMENT_TAG);
            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(MARKET_FRAGMENT_TAG),
                    getSupportFragmentManager().findFragmentByTag(TRADE_FRAGMENT_TAG));
            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(TRADE_FRAGMENT_TAG),
                    getSupportFragmentManager().findFragmentByTag(NEWS_FRAGMENT_TAG));
            CURRENT_FRAGMENT_TAG = NEWS_FRAGMENT_TAG;
        }else {
            FragmentManager manager = getSupportFragmentManager();
            newsFragment = (NewsFragment)manager.getFragment(savedInstanceState,NEWS_FRAGMENT_TAG);
            selectFragment = (SelectFragment)manager.getFragment(savedInstanceState,SELECT_FRAGMENT_TAG);
            marketFragment = (MarketFragment)manager.getFragment(savedInstanceState,MARKET_FRAGMENT_TAG);
            tradeFragment = (TradeFragment)manager.getFragment(savedInstanceState,TRADE_FRAGMENT_TAG);
            savedTagInt = savedInstanceState.getInt(SAVED__TAG_INT,R.id.bottom_nav_news);
            CURRENT_FRAGMENT_TAG = savedInstanceState.getString(SAVED__TAG_STR,NEWS_FRAGMENT_TAG);
            bottomNavigationView.setSelectedItemId(savedTagInt);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.bottom_nav_news:
                        if(!CURRENT_FRAGMENT_TAG.equals(NEWS_FRAGMENT_TAG)){
                            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG),
                                    getSupportFragmentManager().findFragmentByTag(NEWS_FRAGMENT_TAG));
                            CURRENT_FRAGMENT_TAG = NEWS_FRAGMENT_TAG;
                        }
                        break;
                    case R.id.bottom_nav_select:
                        if(!CURRENT_FRAGMENT_TAG.equals(SELECT_FRAGMENT_TAG)){
                            if (getSupportFragmentManager().findFragmentByTag(SELECT_FRAGMENT_TAG) == null){
                                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),selectFragment,R.id.mainFrame,SELECT_FRAGMENT_TAG);
                            }
                            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG),
                                    getSupportFragmentManager().findFragmentByTag(SELECT_FRAGMENT_TAG));
                            CURRENT_FRAGMENT_TAG = SELECT_FRAGMENT_TAG;
                        }
                        break;
                    case R.id.bottom_nav_market:
                        if(!CURRENT_FRAGMENT_TAG.equals(MARKET_FRAGMENT_TAG)){
                            if (getSupportFragmentManager().findFragmentByTag(MARKET_FRAGMENT_TAG) == null){
                                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),marketFragment,R.id.mainFrame,MARKET_FRAGMENT_TAG);
                            }
                            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG),
                                    getSupportFragmentManager().findFragmentByTag(MARKET_FRAGMENT_TAG));
                            CURRENT_FRAGMENT_TAG = MARKET_FRAGMENT_TAG;
                        }
                        break;
                    case R.id.bottom_nav_trade:
                        if(!CURRENT_FRAGMENT_TAG.equals(TRADE_FRAGMENT_TAG)){
                            if (getSupportFragmentManager().findFragmentByTag(TRADE_FRAGMENT_TAG) == null){
                                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),tradeFragment,R.id.mainFrame,TRADE_FRAGMENT_TAG);
                            }
                            ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(CURRENT_FRAGMENT_TAG),
                                    getSupportFragmentManager().findFragmentByTag(TRADE_FRAGMENT_TAG));
                            CURRENT_FRAGMENT_TAG = TRADE_FRAGMENT_TAG;
                        }
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED__TAG_INT, bottomNavigationView.getSelectedItemId());
        outState.putString(SAVED__TAG_STR, CURRENT_FRAGMENT_TAG);
        FragmentManager manager = getSupportFragmentManager();
        manager.putFragment(outState,NEWS_FRAGMENT_TAG,newsFragment);
        manager.putFragment(outState,SELECT_FRAGMENT_TAG,selectFragment);
        manager.putFragment(outState,MARKET_FRAGMENT_TAG,marketFragment);
        manager.putFragment(outState,TRADE_FRAGMENT_TAG,tradeFragment);
    }

    @Override
    public void onBackPressed(){
        if(!BackHandlerHelper.handleBackPress(this)){
            if(!mBackKeyPressed){
                Utils.showToast(this,getString(R.string.click_to_exit),false);
                //Toast.makeText(this,R.string.click_to_exit,Toast.LENGTH_SHORT).show();
                this.mBackKeyPressed = true;
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mBackKeyPressed = false;
                    }
                }, 2000);
            }else {
                this.finish();
                System.exit(0);
            }
        }
    }
}
