package com.zkliu.android.yourstock.mvp.photodetail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.backpress.BackHandlerHelper;
import com.zkliu.android.yourstock.util.ActivityUtils;

import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_FRAG;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_NEWSID;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_NEWSID_FRAG;

/**
 * Created by zekail on 28/6/2017.
 */

public class PhotoDetailActivity extends AppCompatActivity {

    private final static String PHOTO_DETAIL_FRAG = "PHOTO_DETAIL_FRAG";
    private String photoDetailURL;
    private String newsId;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_photo_detail);
        photoDetailURL = getIntent().getStringExtra(PHOTO_DETAIL_URL);
        newsId = getIntent().getStringExtra(PHOTO_DETAIL_URL_NEWSID);

        PhotoDetailFragment photoDetailFragment = (PhotoDetailFragment)getSupportFragmentManager().findFragmentById(R.id.photoDetailFrame);

        if(photoDetailFragment == null){
            photoDetailFragment = PhotoDetailFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString(PHOTO_DETAIL_URL_FRAG,photoDetailURL);
            bundle.putString(PHOTO_DETAIL_URL_NEWSID_FRAG,newsId);
            photoDetailFragment.setArguments(bundle);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),photoDetailFragment,R.id.photoDetailFrame,PHOTO_DETAIL_FRAG);
        }
    }

    @Override
    public void onBackPressed(){
        if(!BackHandlerHelper.handleBackPress(this)){
            this.finish();
            overridePendingTransition(R.anim.slide_backward_in,
                    R.anim.slide_backward_out);
        }
    }
}
