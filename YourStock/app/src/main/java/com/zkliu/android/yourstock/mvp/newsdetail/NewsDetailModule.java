package com.zkliu.android.yourstock.mvp.newsdetail;

import com.zkliu.android.yourstock.mvp.splash.SplashContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by zekail on 8/6/2017.
 */
@Module
public class NewsDetailModule {

    private final NewsDetailContract.View mView;

    public NewsDetailModule(NewsDetailContract.View view){
        mView = view;
    }

    @Provides
    NewsDetailContract.View provideNewsDetailContractView(){
        return mView;
    }
}
