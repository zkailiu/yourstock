package com.zkliu.android.yourstock.data.source.news;

import com.zkliu.android.yourstock.app.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by liuzekai on 25/04/2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NewsRepositoryModule.class})
public interface NewsRepositoryComponent {

    NewsRepository getNewsRepository();
}
