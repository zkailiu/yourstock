package com.zkliu.android.yourstock.mvp.main.news.sub.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;

/**
 * Created by liuzekai on 17/08/2017.
 */

public class News724ViewHolder extends RecyclerView.ViewHolder {

    TextView time, content;

    public News724ViewHolder(final View itemView){
        super(itemView);
        time = (TextView)itemView.findViewById(R.id.news_time);
        content = (TextView)itemView.findViewById(R.id.news_content);
    }

    public void setTime(String time){
        this.time.setText(time);
    }

    public void setContent(String content){
        this.content.setText(content);
    }
}
