package com.zkliu.android.yourstock.retrofit.gson;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;

import java.util.List;

/**
 * Created by liuzekai on 01/05/2017.
 */

public class NonHeadLineNewsGson {
    private List<NewsDTO> nonhead;

    public List<NewsDTO> getNonhead() {
        return nonhead;
    }

    public void setNonhead(List<NewsDTO> nonhead) {
        this.nonhead = nonhead;
    }
}
