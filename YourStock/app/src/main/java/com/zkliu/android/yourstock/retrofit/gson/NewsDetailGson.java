package com.zkliu.android.yourstock.retrofit.gson;

import java.util.List;

/**
 * Created by zekail on 9/6/2017.
 */

public class NewsDetailGson {
    /**
     * body : <div class=...
     * comments : 0
     */

    private String body;
    private int comments;
    private List<String> images;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
