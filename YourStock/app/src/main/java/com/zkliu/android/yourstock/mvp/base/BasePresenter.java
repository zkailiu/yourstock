package com.zkliu.android.yourstock.mvp.base;

/**
 * Created by liuzekai on 2016/12/27.
 */

public interface BasePresenter<V> {

    void onViewAttached(V view);

    void onDestroyed();

    void onViewDetached();
}
