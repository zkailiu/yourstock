package com.zkliu.android.yourstock.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by liuzekai on 2016/12/26.
 */

public class DensityUtil {

    public static int px2dp(Context context, float valueInPx) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (valueInPx / scale + 0.5f);
    }

    public static int dp2px(Context context, float valueInDp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (valueInDp * scale + 0.5f);
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int sp2px(Context context, int valueInSp){
        return (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, valueInSp,
                context.getResources().getDisplayMetrics()) + 0.5f);
    }
}
