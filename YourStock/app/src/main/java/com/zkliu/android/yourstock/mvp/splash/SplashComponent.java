package com.zkliu.android.yourstock.mvp.splash;

import com.zkliu.android.yourstock.mvp.base.FragmentScoped;

import dagger.Component;

/**
 * Created by liuzekai on 2016/12/28.
 */
@FragmentScoped
@Component(modules = SplashModule.class)
public interface SplashComponent {

    void inject(SplashPresenterFactory splashPresenterFactory);
}
