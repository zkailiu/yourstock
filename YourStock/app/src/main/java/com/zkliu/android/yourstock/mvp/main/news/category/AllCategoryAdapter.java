package com.zkliu.android.yourstock.mvp.main.news.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;

import java.util.Arrays;
import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.CATEGORYTEXTMAP;

/**
 * Created by zekail on 8/5/2017.
 */

public class AllCategoryAdapter extends RecyclerView.Adapter {

    public interface onCategoryAllListener {
        void CategoryAllItemClick(View view, int position);
    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private onCategoryAllListener mListener;
    private List<Integer> mCategorylist;

    public void setListener(onCategoryAllListener listener){
        this.mListener = listener;
    }

    public AllCategoryAdapter(Context context, List<Integer> list){
        this.mCategorylist = list;
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype){
        return new NewscategoryAllViewHolder(mLayoutInflater.inflate(R.layout.news_caegory_item,null,false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position){
        final NewscategoryAllViewHolder viewHolder = (NewscategoryAllViewHolder)holder;
        viewHolder.text.setText(CATEGORYTEXTMAP.get(mCategorylist.get(position)));
        viewHolder.text.setOnClickListener(view -> mListener.CategoryAllItemClick(viewHolder.itemView, position));
    }

    @Override
    public int getItemCount(){
        return mCategorylist.size();
    }

    public class NewscategoryAllViewHolder extends RecyclerView.ViewHolder{
        private TextView text;
        public NewscategoryAllViewHolder(View itemView){
            super(itemView);
            text = (TextView)itemView.findViewById(R.id.category_item_text);
        }
    }

    public void showList(){
        Log.i("boboAll", Arrays.toString(mCategorylist.toArray()));
    }
}
