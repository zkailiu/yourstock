package com.zkliu.android.yourstock.mvp.main.news;

import android.support.annotation.NonNull;


import com.zkliu.android.yourstock.data.source.news.NewsRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by liuzekai on 2017/1/2.
 */

public class NewsPresenter implements NewsContract.Presenter {

    @NonNull
    private final CompositeDisposable compositeDisposable;

    @NonNull
    private NewsContract.View mNewsView;

    @NonNull
    private final NewsRepository mNewsRepository;

    @Inject
    NewsPresenter(NewsContract.View newsView, NewsRepository newsRepository){
        mNewsView = newsView;
        mNewsRepository = newsRepository;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewAttached(NewsContract.View view){
        this.mNewsView = view;
    }

    @Override
    public void onViewDetached() {
        mNewsView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }

    @Override
    public void saveCategoryToLocal(List<Integer> list){
        mNewsRepository.saveCategoryToSharedPref(list);
    }

    @Override
    public List<Integer> getCategoryFromLocal(){
        return mNewsRepository.getCategoryFromSharedPref();
    }
}
