package com.zkliu.android.yourstock.widget.sliderbar;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;

/**
 * Created by liuzekai on 22/06/2017.
 */

public class Bar {

    private Paint mBarPaint;
    private Paint mTextPaint;

    private final float mLeftX;
    private final float mRightX;
    private final float mY;
    private final float mPadding;

    private int mSegments;
    private float mTickDistance;
    private final float mTickHeight;
    private final float mTickStartY;
    private final float mTickEndY;

    private Paint mTextSelectedPaint;
    private int selectedItemIndex;
    private String[] texts;
    private int sthDefaultRadius;

    public Bar(float x, float y, float width, int tickCount, float tickHeight,
               float barWidth, int barColor,int textColor, int textSize, int padding) {

        selectedItemIndex = 0;
        texts = new String[]{"小","中","大","特大"};
        sthDefaultRadius = 10;

        mBarPaint = new Paint();
        mBarPaint.setColor(barColor);
        mBarPaint.setStrokeWidth(barWidth);
        mBarPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setColor(textColor);
        mTextPaint.setTextSize(textSize);
        mTextPaint.setAntiAlias(true);

        mTextSelectedPaint = new Paint();
        mTextSelectedPaint.setColor(Color.parseColor("#508cee"));
        mTextSelectedPaint.setTextSize(textSize);
        mTextSelectedPaint.setAntiAlias(true);

        int initTextWidth = (int)getTextWidth(texts[0]);
        float textDistance = width - (float)initTextWidth;

        mLeftX = ((float)(initTextWidth/2)) + x;
        mRightX = mLeftX + textDistance;
        mY = y;
        mPadding = padding;

        mSegments = tickCount - 1;
        mTickDistance = textDistance/(float)mSegments;
        mTickHeight = tickHeight;
        mTickStartY = mY - (mTickHeight / 2.0f);
        mTickEndY = mY + (mTickHeight / 2.0f);
    }

    public void draw(Canvas canvas){
        drawLine(canvas);
        drawTicks(canvas);
    }

    public float getLeftX(){
        return mLeftX;
    }

    public float getRightX(){
        return mRightX;
    }

    public float getNearestTickCoordinate(Thumb thumb){
        final int nearestTickIndex = getNearestTickIndex(thumb);
        return (((float)nearestTickIndex) * mTickDistance) + mLeftX;
    }

    public float getCurrentX(int index){
        return mLeftX + (((float)index)*mTickDistance);
    }

    public int getNearestTickIndex(Thumb thumb){
        return getNearestTickIndex(thumb.getX());
    }

    public int getNearestTickIndex(float x){
        return (int)((x - mLeftX + mTickDistance / 2.0f)/ mTickDistance);
    }

    private void drawLine(Canvas canvas){
        canvas.drawLine(mLeftX, mY, mRightX, mY, mBarPaint);
    }

    public void setSelectedItemIndex(int index){
        selectedItemIndex = index;
    }

    public  float getCurrentXPosition(int index){
        return (((float)index) * mTickDistance) + mLeftX;
    }

    private void drawTicks(Canvas canvas){
        for(int i = 0; i <= mSegments; i++){
            float x = (float)i * mTickDistance + mLeftX;
            canvas.drawLine(x, mTickStartY, x, mTickEndY, mBarPaint);
            String str = texts[i];
            if(!TextUtils.isEmpty(str)){
                if(selectedItemIndex == i){
                    canvas.drawText(str, x - (getTextWidth(str)/2.0f),mTickStartY - mPadding, mTextSelectedPaint);
                }else {
                    canvas.drawText(str, x - (getTextWidth(str)/2.0f),mTickStartY - mPadding, mTextPaint);
                }
            }
        }
    }

    float getTextWidth(String text) {
        return mTextPaint.measureText(text);
    }

    public void destroyResources(){
        if(null != mBarPaint){
            mBarPaint = null;
        }
        if(null != mTextPaint){
            mTextPaint = null;
        }
    }

    public int getClickIndex(float f){
        for (int i = 0; i <= this.mSegments; i++) {
            float a = getTextWidth(texts[i]);
            float f2 = ((((float) i) * mTickDistance) + mLeftX) - (a / 2.0f);
            if (f >= f2 - ((float) this.sthDefaultRadius) && f <= (a + f2) + ((float) this.sthDefaultRadius)) {
                return i;
            }
        }
        return -1;
    }
}

