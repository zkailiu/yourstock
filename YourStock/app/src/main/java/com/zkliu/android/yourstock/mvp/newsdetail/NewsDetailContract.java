package com.zkliu.android.yourstock.mvp.newsdetail;

import com.zkliu.android.yourstock.mvp.base.BasePresenter;

/**
 * Created by zekail on 8/6/2017.
 */

public interface NewsDetailContract {

    interface View{
        void showContent(String content);

        void showProgress();

        void hideProgress();
    }

    interface Presenter extends BasePresenter<NewsDetailContract.View> {
        void getNewsContent(String id,String category);

        int getFontSize();

        void saveFontSize(int size);
    }
}
