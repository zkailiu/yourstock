package com.zkliu.android.yourstock.mvp.photodetail.sub;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.util.ImageLoader;
import com.zkliu.android.yourstock.widget.scaleImageView.ScaleImageView;

/**
 * Created by liuzekai on 30/06/2017.
 */

public class PhotoDetailSubFragment extends Fragment{

    private String imgUrl;

    private ScaleImageView mPhotoView;
    private ProgressBar mProgressBar;

    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasLoaded;


    public static PhotoDetailSubFragment newInstance(String url){
        PhotoDetailSubFragment subFragment = new PhotoDetailSubFragment();
        subFragment.setUrl(url);
        return subFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_sub_photo_detail, container, false);
        mPhotoView = (ScaleImageView) view.findViewById(R.id.sub_photo_view);
        mProgressBar = (ProgressBar)view.findViewById(R.id.sub_photo_progress_bar);
        isPrepared = true;
        lazyLoad();
        return view;
    }

    private void setUrl(String url){
        imgUrl = url;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        if(getUserVisibleHint()){
            isVisible = true;
            onVisible();
        }else {
            isVisible = false;
            onInVisible();
        }
    }

    private void onVisible(){
        lazyLoad();
    }

    private void lazyLoad(){
        if(!isVisible || !isPrepared || hasLoaded){
            return;
        }
        hasLoaded = true;
        mProgressBar.setVisibility(View.VISIBLE);
        ImageLoader.loadWithOutScale(getActivity(), imgUrl, mPhotoView, new Callback() {
            @Override
            public void onSuccess() {
                mProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onError() {
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void onInVisible(){
    }


}
