package com.zkliu.android.yourstock.data.source.news;

import android.support.annotation.NonNull;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;
import com.zkliu.android.yourstock.data.source.Local;
import com.zkliu.android.yourstock.data.source.Remote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import static com.zkliu.android.yourstock.util.Constants.NEWS_724;
import static com.zkliu.android.yourstock.util.Constants.NEWS_FINANCE;
import static com.zkliu.android.yourstock.util.Constants.NEWS_HK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_STOCK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_US;
import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by liuzekai on 09/04/2017.
 */
@Singleton
public class NewsRepository implements NewsDataSource {

    @NonNull
    private final NewsDataSource newsRemoteDataSource;

    @NonNull
    private final NewsDataSource newsLocalDaraSource;

    private static final Map<Integer, LinkedHashMap<String, NewsDTO>> cachedNewses;
    static {
        Map<Integer, LinkedHashMap<String, NewsDTO>> cachedNewsesInit = new HashMap<>();
        cachedNewsesInit.put(NEWS_FINANCE, new LinkedHashMap<>());
        cachedNewsesInit.put(NEWS_STOCK, new LinkedHashMap<>());
        cachedNewsesInit.put(NEWS_US, new LinkedHashMap<>());
        cachedNewsesInit.put(NEWS_HK, new LinkedHashMap<>());
        cachedNewsesInit.put(NEWS_724, new LinkedHashMap<>());
        cachedNewses = Collections.unmodifiableMap(cachedNewsesInit);
    }
    @Inject
    NewsRepository(@Remote @NonNull NewsDataSource newsRemoteDataSource,
                   @Local @NonNull NewsDataSource newsLocalDaraSource){
        checkNotNull(newsRemoteDataSource);
        checkNotNull(newsLocalDaraSource);
        this.newsRemoteDataSource = newsRemoteDataSource;
        this.newsLocalDaraSource = newsLocalDaraSource;
    }

    @Override
    public Observable<List<NewsDTO>> getHeadLindNewsList(boolean isRemote, int category){
        // Respond immediately with cache if available and not dirty
        if(cachedNewses.get(category).size() != 0 && !isRemote){
            return Observable.fromCallable(new Callable<List<NewsDTO>>() {
                @Override
                public List<NewsDTO> call() throws Exception {
                    List<NewsDTO> arraylist = new ArrayList<NewsDTO>(cachedNewses.get(category).values());
                    return arraylist;
                }
            });
        }

        if(isRemote){
            return getAndSaveRemoteHeadLindNewses(category);
        }else {
            return getAndSaveLocalHeadLindNewses(category);
        }
    }

    private Observable<List<NewsDTO>> getAndSaveLocalHeadLindNewses(int category){
        return newsLocalDaraSource.getHeadLindNewsList(false,category)
                .concatMap(new Function<List<NewsDTO>, Observable<List<NewsDTO>>>() {
                    @Override
                    public Observable<List<NewsDTO>> apply(@io.reactivex.annotations.NonNull List<NewsDTO> newses) throws Exception {
                        return Observable.fromIterable(newses).doOnNext(news ->{
                            cachedNewses.get(category).put(news.getNewsId(),news);
                        }).toList().toObservable();
                    }
                });
    }

    private Observable<List<NewsDTO>> getAndSaveRemoteHeadLindNewses(int category){
        return newsRemoteDataSource.getHeadLindNewsList(true,category)
                .doOnNext(newsDTOs -> saveHeadLineNewsToDB(newsDTOs))
                .concatMap(new Function<List<NewsDTO>, Observable<List<NewsDTO>>>(){
                    @Override
                    public Observable<List<NewsDTO>> apply(List<NewsDTO> newses) throws Exception{
                        return Observable.fromIterable(newses).doOnNext(news ->{
                            cachedNewses.get(category).put(news.getNewsId(),news);
                        }).toList().toObservable();
                    }
                })
                .doOnComplete(()->{
                    });
    }

    @Override
    public void saveHeadLineNewsToDB(@NonNull List<NewsDTO> newsDTOs){
        checkNotNull(newsDTOs);
        newsLocalDaraSource.saveHeadLineNewsToDB(newsDTOs);
    }

    @Override
    public Observable<List<NewsDTO>> getNonHeadLineNewsList(int category){
        return newsRemoteDataSource.getNonHeadLineNewsList(category);
    }

    @Override
    public void saveCategoryToSharedPref(List<Integer> list){
        newsLocalDaraSource.saveCategoryToSharedPref(list);
    }

    @Override
    public List<Integer> getCategoryFromSharedPref(){
        return newsLocalDaraSource.getCategoryFromSharedPref();
    }

    @Override
    public Observable<String> getNewsDetailContent(String id,String category){
        return Observable.zip(newsLocalDaraSource.getNewsDetailContent(id,category),newsRemoteDataSource.getNewsDetailContent(id,category),
                (local, remote)->{
                    String content = local.replace("[NEWSCONTENTS]",remote);
                    return content;
                });
    }

    @Override
    public int getFontSizeFromSharedPref(){
        return newsLocalDaraSource.getFontSizeFromSharedPref();
    }

    @Override
    public void saveFontSizeToSharedPref(int size){
        newsLocalDaraSource.saveFontSizeToSharedPref(size);
    }

    @Override
    public List<String> getImgList(String id){
        return newsRemoteDataSource.getImgList(id);
    }

    @Override
    public Observable<String> saveImg(final String url){
        return newsLocalDaraSource.saveImg(url);
    }

}
