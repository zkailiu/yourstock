package com.zkliu.android.yourstock.mvp.splash;

import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;

import javax.inject.Inject;

/**
 * Created by liuzekai on 2017/1/12.
 */

public class SplashPresenterFactory implements BasePresenterFactory<SplashPresenter> {

    private final SplashFragment splashFragment;
    public SplashPresenterFactory(SplashFragment mSplashFragment){
        splashFragment = mSplashFragment;
    }

    @Inject
    SplashPresenter mSplashPresenter;

    @Override
    public SplashPresenter create(){
        DaggerSplashComponent.builder()
                .splashModule(
                        new SplashModule(splashFragment))
                .build().inject(this);
        return mSplashPresenter;
    };
}
