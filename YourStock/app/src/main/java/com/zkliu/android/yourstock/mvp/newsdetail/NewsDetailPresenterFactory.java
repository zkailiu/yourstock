package com.zkliu.android.yourstock.mvp.newsdetail;

import com.zkliu.android.yourstock.app.StockApplication;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;

import javax.inject.Inject;

/**
 * Created by zekail on 8/6/2017.
 */

public class NewsDetailPresenterFactory implements BasePresenterFactory<NewsDetailPresenter> {

    private final NewsDetailFragment ndFragment;
    public NewsDetailPresenterFactory(NewsDetailFragment newsDetailFragment){
        ndFragment = newsDetailFragment;
    }

    @Inject
    NewsDetailPresenter ndPresenter;

    @Override
    public NewsDetailPresenter create(){
        DaggerNewsDetailComponent.builder()
                .newsDetailModule(new NewsDetailModule(ndFragment))
                .newsRepositoryComponent(((StockApplication)ndFragment.getActivity().getApplication()).getNewsRepositoryComponent())
                .build().inject(this);
        return ndPresenter;
    }
}
