package com.zkliu.android.yourstock.mvp.newsdetail;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.backpress.BackHandlerHelper;
import com.zkliu.android.yourstock.util.ActivityUtils;

import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_CATEGORY;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_CATEGORY_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_ID;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_ID_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_IMGURL;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_IMGURL_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_TITLE;
import static com.zkliu.android.yourstock.util.Constants.NEWS_DETAIL_TITLE_TO_FRAG;
import static com.zkliu.android.yourstock.util.Constants.VIEW_INFO_EXTRA;
import static com.zkliu.android.yourstock.util.Constants.VIEW_INFO_EXTRA_FRAG;

public class NewsDetailActivity extends AppCompatActivity {

    private final static String NEWS_DETAIL_TAG = "NEWS_DETAIL_FRAG";

    private String newsDetailTitle;
    private String newsDetailID;
    private String newsDetailURL;
    private String newsDetailCATEGORY;
    private Bundle locationInfoBundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            setTheme(R.style.Transparent);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_news_detail);

        newsDetailTitle = getIntent().getStringExtra(NEWS_DETAIL_TITLE);
        newsDetailID = getIntent().getStringExtra(NEWS_DETAIL_ID);
        newsDetailURL = getIntent().getStringExtra(NEWS_DETAIL_IMGURL);
        newsDetailCATEGORY = getIntent().getStringExtra(NEWS_DETAIL_CATEGORY);
        locationInfoBundle = getIntent().getBundleExtra(VIEW_INFO_EXTRA);

        NewsDetailFragment newsDetailFragment = (NewsDetailFragment)getSupportFragmentManager().findFragmentById(R.id.newsDetailFrame);

        if(newsDetailFragment == null){
            newsDetailFragment = NewsDetailFragment.newInstance();
            if(newsDetailTitle != null && newsDetailID != null && newsDetailURL != null){
                Bundle bundle = new Bundle();
                bundle.putString(NEWS_DETAIL_TITLE_TO_FRAG,newsDetailTitle);
                bundle.putString(NEWS_DETAIL_ID_TO_FRAG,newsDetailID);
                bundle.putString(NEWS_DETAIL_IMGURL_TO_FRAG,newsDetailURL);
                bundle.putString(NEWS_DETAIL_CATEGORY_TO_FRAG,newsDetailCATEGORY);
                bundle.putBundle(VIEW_INFO_EXTRA_FRAG,locationInfoBundle);
                newsDetailFragment.setArguments(bundle);
            }
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),newsDetailFragment,R.id.newsDetailFrame,NEWS_DETAIL_TAG);
        }
    }

    @Override
    public void onBackPressed(){
        if(!BackHandlerHelper.handleBackPress(this)){
            this.finish();
            overridePendingTransition(R.anim.slide_backward_in,
                    R.anim.slide_backward_out);
        }
    }

}
