package com.zkliu.android.yourstock.data.source.news.remote;


import android.text.TextUtils;

import com.zkliu.android.yourstock.data.entity.news.NewsDTO;
import com.zkliu.android.yourstock.data.source.news.NewsDataSource;
import com.zkliu.android.yourstock.retrofit.RetrofitClient;
import com.zkliu.android.yourstock.retrofit.RetrofitService;
import com.zkliu.android.yourstock.retrofit.gson.HeadLineNewsGson;
import com.zkliu.android.yourstock.retrofit.gson.NewsDetailGson;
import com.zkliu.android.yourstock.retrofit.gson.NonHeadLineNewsGson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.zkliu.android.yourstock.util.Constants.NEWS_724;
import static com.zkliu.android.yourstock.util.Constants.NEWS_FINANCE;
import static com.zkliu.android.yourstock.util.Constants.NEWS_HK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_STOCK;
import static com.zkliu.android.yourstock.util.Constants.NEWS_US;

/**
 * Created by liuzekai on 09/04/2017.
 */
@Singleton
public class NewsRemoteDataSource implements NewsDataSource {

    public NewsRemoteDataSource(){}

    private static final Map<Integer,String> categoryMap;
    private static Map<Integer,Integer> nhlnListIndex;
    private static Map<Integer,Boolean> hasLoadAllNHLNews;
    private static Map<String,List<String>> imgList = new HashMap<>();;
    static {
        Map<Integer,String> categoryMapInit = new HashMap<Integer, String>();
        categoryMapInit.put(NEWS_FINANCE,"Finance");
        categoryMapInit.put(NEWS_STOCK,"Stock");
        categoryMapInit.put(NEWS_US,"USStock");
        categoryMapInit.put(NEWS_HK,"HKStock");
        categoryMapInit.put(NEWS_724,"724");
        categoryMap = Collections.unmodifiableMap(categoryMapInit);

        nhlnListIndex = new HashMap<>();
        nhlnListIndex.put(NEWS_FINANCE,0);
        nhlnListIndex.put(NEWS_STOCK,0);
        nhlnListIndex.put(NEWS_US,0);
        nhlnListIndex.put(NEWS_HK,0);
        nhlnListIndex.put(NEWS_724,0);

        hasLoadAllNHLNews = new HashMap<>();
        hasLoadAllNHLNews.put(NEWS_FINANCE,false);
        hasLoadAllNHLNews.put(NEWS_STOCK,false);
        hasLoadAllNHLNews.put(NEWS_US,false);
        hasLoadAllNHLNews.put(NEWS_HK,false);
        hasLoadAllNHLNews.put(NEWS_724,false);
    }

    private static Map<Integer,ArrayList<String>> categoryMapNHLNList = new HashMap<>();

    // Not required because the {@link NewsRepository} handles the logic
    // of refreshing the packages from all available data source
    @Override
    public Observable<List<NewsDTO>> getHeadLindNewsList(boolean isRemote, int category){
        return RetrofitClient.getInstance()
                .create(RetrofitService.class)
                .getHeadlineNews(categoryMap.get(category))
                .subscribeOn(Schedulers.io())
                .doOnNext(headLineNewsGson -> {
                    categoryMapNHLNList.put(category,new ArrayList<String>(Arrays.asList(headLineNewsGson.getNonhead().split(","))));
                })
                .map(new Function<HeadLineNewsGson, List<NewsDTO>>() {
                    @Override
                    public List<NewsDTO> apply(@NonNull HeadLineNewsGson hlngson) throws Exception {
                        return hlngson.getHead();
                    }
                })
                .doOnComplete(()->{
                    nhlnListIndex.put(category,0);
                    hasLoadAllNHLNews.put(category,false);
                });
    }

    @Override
    public void saveHeadLineNewsToDB(@NonNull List<NewsDTO> newsDTOs){
        //do nothing here
    }

    @Override
    public Observable<List<NewsDTO>> getNonHeadLineNewsList(int category){
        int newsStep = 15;
        String idlist;
        if(categoryMapNHLNList.get(category) != null){
            if(!hasLoadAllNHLNews.get(category)){
                if(nhlnListIndex.get(category)+newsStep<=categoryMapNHLNList.get(category).size()){
                    idlist = TextUtils.join(",",categoryMapNHLNList.get(category).subList(nhlnListIndex.get(category), nhlnListIndex.get(category)+newsStep));
                }else {
                    idlist = TextUtils.join(",",categoryMapNHLNList.get(category).subList(nhlnListIndex.get(category),categoryMapNHLNList.get(category).size()));
                }
                return RetrofitClient.getInstance()
                        .create(RetrofitService.class)
                        .getNonHeadlineNews(categoryMap.get(category),idlist)
                        .subscribeOn(Schedulers.io())
                        .map(new Function<NonHeadLineNewsGson, List<NewsDTO>>() {
                            @Override
                            public List<NewsDTO> apply(@NonNull NonHeadLineNewsGson nonHeadLineNewsGson) throws Exception {
                                return nonHeadLineNewsGson.getNonhead();
                            }
                        })
                        .doOnComplete(()-> {
                            nhlnListIndex.put(category, nhlnListIndex.get(category)+newsStep);
                            if(nhlnListIndex.get(category)>=categoryMapNHLNList.get(category).size()){
                                hasLoadAllNHLNews.put(category,true);
                            }
                        });
            }else {
                return Observable.error(new Throwable("1 yi jing mei you xin shu ju le"));
            }
        }else {
            return Observable.error(new Throwable("2 jian cha wang luo, shua xin chong shi"));
        }
    }

    @Override
    public void saveCategoryToSharedPref(List<Integer> list){
        //do nothing here
    }

    @Override
    public List<Integer> getCategoryFromSharedPref(){
        return null;//do nothing here
    }

    @Override
    public Observable<String> getNewsDetailContent(String id, String category){
        return RetrofitClient.getInstance()
                .create(RetrofitService.class)
                .getNewsDetail(id)
                .delay(800, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .doOnNext(ndgson -> {
                    imgList.put(id,ndgson.getImages());
                })
                .map(new Function<NewsDetailGson, String>() {
                    @Override
                    public String apply(@io.reactivex.annotations.NonNull NewsDetailGson ndgson) throws Exception {
                        String content = ndgson.getBody();
                        return getInnerContent(content, category);
                    }
                });
    }

    @Override
    public int getFontSizeFromSharedPref(){
        return 1;
    };

    @Override
    public void saveFontSizeToSharedPref(int size){
        //do nothing
    }

    public String getInnerContent(String str, String category){
        String subString;
        if(category.equals("Finance")||category.equals("Stock")){
            int index1 = str.indexOf("class");
            String subString1 = str.substring(index1);
            int index2 = subString1.indexOf("<");
            subString = subString1.substring(index2,subString1.length()-20);
        }else {
            int index1 = str.indexOf("body");
            String subString1 = str.substring(index1);
            int index2 = subString1.indexOf("<");
            subString = subString1.substring(index2,subString1.length()-14);
        }
        return subString;
    }

    @Override
    public List<String> getImgList(String id){
        return imgList.get(id);
    }

    @Override
    public Observable<String> saveImg(final String url){
        //do nothing
        return null;
    }

}
