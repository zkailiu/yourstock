package com.zkliu.android.yourstock.mvp.main.news.category;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;

import java.util.Arrays;
import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.CATEGORYTEXTMAP;

/**
 * Created by zekail on 8/5/2017.
 */

public class ChoseCategoryAdapter extends RecyclerView.Adapter implements MoveAndDismissListener {

    public interface onCategoryChoseListener {
        void CategoryChoseItemClick(View view, int position);
    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private onCategoryChoseListener mListener;
    private List<Integer> mCategorylist;

    public void setListener(onCategoryChoseListener listener){
        this.mListener = listener;
    }

    public ChoseCategoryAdapter(Context context, List<Integer> list){
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mCategorylist = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        return new NewsCategoryChoseViewHolder(mLayoutInflater.inflate(R.layout.news_caegory_item,null,false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position){
        final NewsCategoryChoseViewHolder viewHolder = (NewsCategoryChoseViewHolder)holder;
        if (position==0){
            viewHolder.text.setBackgroundResource(R.drawable.news_category_item_background_first);
            viewHolder.text.setTextColor(Color.WHITE);
        }
        viewHolder.text.setText(CATEGORYTEXTMAP.get(mCategorylist.get(position)));
        viewHolder.text.setOnClickListener(view -> {
            mListener.CategoryChoseItemClick(viewHolder.itemView, position);
        });
        viewHolder.text.setOnLongClickListener(view -> {
            return true;
        });
    }

    @Override
    public int getItemCount(){
        return mCategorylist.size();
    }

    public class NewsCategoryChoseViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout ncll;
        private TextView text;
        public NewsCategoryChoseViewHolder(View itemView){
            super(itemView);
            ncll =(LinearLayout)itemView.findViewById(R.id.news_category_ll);
            text = (TextView)itemView.findViewById(R.id.category_item_text);
        }
        public void setBackground(boolean isDefault){
            if(isDefault){
                text.setBackgroundResource(R.drawable.news_category_item_background);
                ncll.startAnimation(AnimationUtils.loadAnimation(mContext,R.anim.news_category_scale_normal));
            }else {
                text.setBackgroundResource(R.drawable.news_category_item_background_drag);
                ncll.startAnimation(AnimationUtils.loadAnimation(mContext,R.anim.news_category_scale_large));
            }
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition){
        int item = mCategorylist.get(fromPosition);
        mCategorylist.remove(fromPosition);
        mCategorylist.add(toPosition,item); //use for gridlayout
//        Collections.swap(mCategorylist,fromPosition,toPosition);  maybe used in lineadlayout
        notifyItemMoved(fromPosition,toPosition);
        return true;
    }

    public List<Integer> getList(){
        Log.i("boboChose", Arrays.toString(mCategorylist.toArray()));
        return mCategorylist;
    }
}
