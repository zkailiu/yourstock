package com.zkliu.android.yourstock.mvp.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.util.ActivityUtils;


public class SplashActivity extends AppCompatActivity {

    private final static String FRAG_TAG = "SPLASH_FRAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash);

        SplashFragment splashFragment = (SplashFragment)getSupportFragmentManager().findFragmentById(R.id.splashFrame);

        if(splashFragment == null){
            splashFragment = SplashFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),splashFragment,R.id.splashFrame,FRAG_TAG);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
