package com.zkliu.android.yourstock.mvp.base.backpress;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import java.util.List;

/**
 * Created by liuzekai on 13/05/2017.
 */

public class BackHandlerHelper {

    public static boolean handleBackPress(FragmentManager fragmentManager){
        List<Fragment> fragments = fragmentManager.getFragments();

        if(fragments == null) return false;

        for(int i = fragments.size() - 1; i >= 0; i--){
            Fragment child = fragments.get(i);

            if(isFragmentBackHandled(child)){
                return true;
            }
        }

        return false;
    }

    public static boolean handleBackPress(Fragment fragment){
        return handleBackPress(fragment.getChildFragmentManager());
    }

    public static boolean handleBackPress(FragmentActivity fragmentActivity){
        return handleBackPress(fragmentActivity.getSupportFragmentManager());
    }

    public static boolean handleBackPress(ViewPager viewPager){
        if(viewPager == null ) return false;

        PagerAdapter adapter = viewPager.getAdapter();

        if(adapter == null) return false;

        int currentItem = viewPager.getCurrentItem();
        Fragment fragment;
        if(adapter instanceof FragmentPagerAdapter){
            fragment = ((FragmentPagerAdapter)adapter).getItem(currentItem);
        }else if(adapter instanceof FragmentStatePagerAdapter){
            fragment = ((FragmentStatePagerAdapter)adapter).getItem(currentItem);
        }else {
            fragment = null;
        }
        return isFragmentBackHandled(fragment);
    }

    public static boolean isFragmentBackHandled(Fragment fragment){
        return fragment != null
                && fragment.isVisible()
                && fragment.getUserVisibleHint() //for viewpager
                && fragment instanceof FragmentBackHandler
                && ((FragmentBackHandler)fragment).onBackPressed();
    }
}
