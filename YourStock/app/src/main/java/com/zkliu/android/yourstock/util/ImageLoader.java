package com.zkliu.android.yourstock.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;

/**
 * Created by zekail on 8/6/2017.
 */

public class ImageLoader {

    private static boolean showLogging = true;

    public static void loadWithScale(Context context, String url, Target targetImageView, int scaleX, int scaleY, Drawable placeHolder) {
        Picasso mPicasso = Picasso.with(context);
        mPicasso.setLoggingEnabled(showLogging);
        mPicasso.load(url)
                .resize(DensityUtil.dp2px(context,scaleX), DensityUtil.dp2px(context,scaleY))
                .error(placeHolder)
                .placeholder(placeHolder)
                .into(targetImageView);
    }

    public static void loadWithOutScale(Context context, String url, ImageView imageView, Callback callback) {
        Picasso mPicasso = Picasso.with(context);
        mPicasso.setLoggingEnabled(showLogging);
        mPicasso.load(url)
                .into(imageView, callback);
    }

    public static Bitmap getImage(Context context, String url){
        Picasso mPicasso = Picasso.with(context);
        mPicasso.setLoggingEnabled(showLogging);
        Bitmap bitmap = null;
        try {
            bitmap = mPicasso.with(context)
                    .load(url)
                    .get();
        }catch (IOException e){
            e.printStackTrace();
        }
        return bitmap;
    }
}
