package com.zkliu.android.yourstock.mvp.splash;

import dagger.Module;
import dagger.Provides;

/**
 * Created by liuzekai on 2016/12/28.
 */

@Module
public class SplashModule {

    private final SplashContract.View mView;

    public SplashModule(SplashContract.View view){
        mView = view;
    }

    @Provides
    SplashContract.View provideSplashContractView(){
        return mView;
    }
}
