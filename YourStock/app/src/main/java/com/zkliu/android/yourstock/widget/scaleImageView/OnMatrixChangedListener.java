package com.zkliu.android.yourstock.widget.scaleImageView;

import android.graphics.RectF;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnMatrixChangedListener {

    void onMatrixChanged(RectF rect);

}
