package com.zkliu.android.yourstock.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuzekai on 2016/12/26.
 */

public final class Constants {

    public final static int RADIUS = 10;

    public final static int NEWS_FINANCE = 101;
    public final static int NEWS_STOCK = 102;
    public final static int NEWS_US = 103;
    public final static int NEWS_HK = 104;
    public final static int NEWS_724 = 105;

    public static final Map<Integer,String> CATEGORYTEXTMAP;
    static {
        Map<Integer,String> categoryMapInit = new HashMap<Integer, String>();
        categoryMapInit.put(NEWS_FINANCE,"要闻");
        categoryMapInit.put(NEWS_STOCK,"股票");
        categoryMapInit.put(NEWS_US,"美股");
        categoryMapInit.put(NEWS_HK,"港股");
        categoryMapInit.put(NEWS_724,"7x24");
        CATEGORYTEXTMAP = Collections.unmodifiableMap(categoryMapInit);
    }

    public final static String NEWS_DETAIL_TITLE = "NEWS_DETAIL_TITLE";
    public final static String NEWS_DETAIL_ID = "NEWS_DETAIL_ID";
    public final static String NEWS_DETAIL_IMGURL = "NEWS_DETAIL_IMGURL";
    public final static String NEWS_DETAIL_CATEGORY = "NEWS_DETAIL_CATEGORY";


    public final static String NEWS_DETAIL_TITLE_TO_FRAG = "NEWS_DETAIL_TITLE_TO_FRAG";
    public final static String NEWS_DETAIL_ID_TO_FRAG = "NEWS_DETAIL_ID_TO_FRAG";
    public final static String NEWS_DETAIL_IMGURL_TO_FRAG = "NEWS_DETAIL_IMGURL_TO_FRAG";
    public final static String NEWS_DETAIL_CATEGORY_TO_FRAG = "NEWS_DETAIL_CATEGORY_TO_FRAG";


    public final static String PHOTO_DETAIL_URL = "PHOTO_DETAIL_URL";
    public final static String PHOTO_DETAIL_URL_FRAG = "PHOTO_DETAIL_URL";
    public final static String PHOTO_DETAIL_URL_NEWSID = "PHOTO_DETAIL_URL_NEWSID";
    public final static String PHOTO_DETAIL_URL_NEWSID_FRAG = "PHOTO_DETAIL_URL_NEWSID_FRAG";

    //animation for api < lollipop
    public final static String VIEW_INFO_EXTRA = "VIEW_INFO_EXTRA";
    public final static String VIEW_INFO_EXTRA_FRAG = "VIEW_INFO_EXTRA_FRAG";

    public final static String PROPNAME_SCREENLOCATION_LEFT = "PROPNAME_SCREENLOCATION_LEFT";
    public final static String PROPNAME_SCREENLOCATION_TOP = "PROPNAME_SCREENLOCATION_TOP";
    public final static String PROPNAME_WIDTH = "PROPNAME_WIDTH";
    public final static String PROPNAME_HEIGHT = "PROPNAME_HEIGHT";
}
