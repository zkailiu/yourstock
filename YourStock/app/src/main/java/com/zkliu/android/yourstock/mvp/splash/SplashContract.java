package com.zkliu.android.yourstock.mvp.splash;


import com.zkliu.android.yourstock.mvp.base.BasePresenter;


/**
 * Created by liuzekai on 2016/12/27.
 */

public interface SplashContract {

    interface View {

        void startBottomAnimation();

        void startBezierTransAnimation();

        void startBezierRotateAnimation();

        void startShowCenterLogoCover();

        void startBottomAnimationReversal();

        void cancelCurrentAnimation();
    }

    interface Presenter extends BasePresenter<SplashContract.View> {
        void start();

        void animationFinished(int stepID);

    }

    int ANIMATIONSTEPZERO = 0;

    int ANIMATIONSTEPONE = 1;

    int ANIMATIONSTEPTWO = 2;

    int ANIMATIONSTEPTHREE = 3;

    int ANIMATIONSTEPFOUR = 4;
}
