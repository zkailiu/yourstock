package com.zkliu.android.yourstock.mvp.photodetail;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.base.backpress.FragmentBackHandler;
import com.zkliu.android.yourstock.mvp.photodetail.adapter.PhotoViewAdapter;
import com.zkliu.android.yourstock.mvp.photodetail.sub.PhotoDetailSubFragment;
import com.zkliu.android.yourstock.util.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_FRAG;
import static com.zkliu.android.yourstock.util.Constants.PHOTO_DETAIL_URL_NEWSID_FRAG;

/**
 * Created by zekail on 28/6/2017.
 */

public class PhotoDetailFragment extends BaseFragment<PhotoDetailPresenter, PhotoDetailContract.View> implements PhotoDetailContract.View,FragmentBackHandler {
    private PhotoDetailContract.Presenter mPresenter;
    private TextView imgIndexTV;
    private ImageView mDonwload;

    private String imgurl;
    private String newsId;
    private List<String> imgList;

    private ViewPager mPager;
    private PhotoViewAdapter mAdapter;
    private List<PhotoDetailSubFragment> mPhotoDetailFragmentList = new ArrayList<>();
    private int mCurrentPage;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 111;


    public PhotoDetailFragment(){
    }

    public static PhotoDetailFragment newInstance(){
        return new PhotoDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        View root = inflater.inflate(R.layout.frag_photo_detail,container,false);
        imgurl = getArguments().getString(PHOTO_DETAIL_URL_FRAG);
        newsId = getArguments().getString(PHOTO_DETAIL_URL_NEWSID_FRAG);
        imgIndexTV = (TextView)root.findViewById(R.id.photo_index_indicator);
        mDonwload = (ImageView)root.findViewById(R.id.photo_download_img);
        mDonwload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED){
                    if(!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                        showMessageForPermission("You need allow access to storage",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }
                                });
                    }
                    ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                }else {
                    saveImg();
                }
            }
        });

        mPager = (ViewPager)root.findViewById(R.id.viewPager_photos);
        return root;
    }

    private void showMessageForPermission(String message, DialogInterface.OnClickListener listener){
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK",listener)
                .setNegativeButton("Cancel",null)
                .create()
                .show();
    }

    private void saveImg(){
        if(mPresenter != null){
            mPresenter.saveImg(imgList.get(mCurrentPage));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    saveImg();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        }
    }

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new PhotoDetailresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull PhotoDetailPresenter presenter) {
        this.mPresenter = presenter;
        imgList = mPresenter.getImgList(newsId);
        setCurrentIndex(imgurl);
        createFragment(imgList);
        initViewPager();
    }

    @Override
    public boolean onBackPressed(){
        return false;
    }

    private void setCurrentIndex(String imgurl){
        mCurrentPage = imgList.indexOf(imgurl);
        imgIndexTV.setText((mCurrentPage+1)+"/"+imgList.size());
    }

    private void createFragment(List<String> list) {
        mPhotoDetailFragmentList.clear();
        for (String url : list) {
            PhotoDetailSubFragment fragment = PhotoDetailSubFragment.newInstance(url);
            mPhotoDetailFragmentList.add(fragment);
        }
    }

    private void initViewPager(){

        mAdapter = new PhotoViewAdapter(getChildFragmentManager(),mPhotoDetailFragmentList);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(mAdapter.getCount()-1);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                imgIndexTV.setText((position+1)+"/"+imgList.size());
                mCurrentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mPager.setCurrentItem(mCurrentPage);
    }

    @Override
    public void showSaveResult(String result){
        Utils.showToast(getActivity(),result,false);
    }
}
//http://blog.csdn.net/lvshaorong/article/details/52623027
//https://github.com/wingjay/BlurImageView