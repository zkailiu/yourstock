package com.zkliu.android.yourstock.mvp.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by liuzekai on 06/07/2017.
 */
@Module
public class LoginModule {

    private final LoginContract.View mView;

    public LoginModule(LoginContract.View view){
        mView = view;
    }

    @Provides
    LoginContract.View provideLoginContractView(){
        return mView;
    }
}
