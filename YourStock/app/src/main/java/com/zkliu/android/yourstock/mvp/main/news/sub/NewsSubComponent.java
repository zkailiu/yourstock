package com.zkliu.android.yourstock.mvp.main.news.sub;

import com.zkliu.android.yourstock.data.source.news.NewsRepositoryComponent;
import com.zkliu.android.yourstock.mvp.base.FragmentScoped;
import com.zkliu.android.yourstock.mvp.main.news.NewsModule;

import dagger.Component;

/**
 * Created by liuzekai on 05/05/2017.
 */
@FragmentScoped
@Component(modules = NewsSubModule.class,
        dependencies = NewsRepositoryComponent.class)
public interface NewsSubComponent {
    void inject(NewsSubPresenterFactory newsSubPresenterFactory);

}
