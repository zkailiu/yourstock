package com.zkliu.android.yourstock.mvp.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.BaseFragment;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;
import com.zkliu.android.yourstock.mvp.base.backpress.FragmentBackHandler;
import com.zkliu.android.yourstock.util.ActivityUtils;

import java.util.regex.Pattern;

import static com.zkliu.android.yourstock.mvp.login.LoginActivity.LOGIN_FRAG;
import static com.zkliu.android.yourstock.mvp.login.LoginActivity.REGISTER_FRAG;

/**
 * Created by liuzekai on 06/07/2017.
 */

public class LoginFragment extends BaseFragment<LoginPresenter, LoginContract.View> implements LoginContract.View,FragmentBackHandler{

    Toolbar mToolbar;
    EditText mailEdit;
    EditText passEdit;
    TextView forgetPassText;
    Button registerBtn;
    Button loginBtn;
    ProgressBar mProgressBar;

    private LoginContract.Presenter mPresenter;

    public LoginFragment(){
    }

    public static LoginFragment newInstance(){
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View root = inflater.inflate(R.layout.frag_login,container,false);
        mToolbar = (Toolbar)root.findViewById(R.id.login_toolbar);
        mToolbar.setNavigationOnClickListener(view->{
            getActivity().onBackPressed();
        });
        mToolbar.setTitle(R.string.login);

        mailEdit = (EditText)root.findViewById(R.id.mail);
        mailEdit.addTextChangedListener(mTextWatcher);

        passEdit = (EditText)root.findViewById(R.id.password);
        passEdit.addTextChangedListener(mTextWatcher);

        forgetPassText = (TextView)root.findViewById(R.id.forget_password);

        registerBtn = (Button)root.findViewById(R.id.register);
        registerBtn.setOnClickListener(v -> {
            ActivityUtils.switchBetweenFragment(getActivity().getSupportFragmentManager(),
                    getActivity().getSupportFragmentManager().findFragmentByTag(LOGIN_FRAG),
                    getActivity().getSupportFragmentManager().findFragmentByTag(REGISTER_FRAG),ActivityUtils.FORWARD);
        });

        loginBtn = (Button)root.findViewById(R.id.login);
        if(mailEdit.getText().toString().equals("") || passEdit.getText().toString().equals("")){
            loginBtn.setEnabled(false);
        }
        loginBtn.setOnClickListener(v -> {
            if(isInputValid()){
                mPresenter.login(mailEdit.getText().toString().trim(),passEdit.getText().toString().trim());
                InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(registerBtn.getWindowToken(), 0);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        });
        mProgressBar = (ProgressBar)root.findViewById(R.id.login_progress);
        return root;
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if(mailEdit.getText().toString().trim().length()!=0 && passEdit.getText().toString().trim().length()!=0){
                loginBtn.setEnabled(true);
            }else {
                loginBtn.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @NonNull
    @Override
    protected BasePresenterFactory getPresenterFactory() {
        return new LoginPresenterFactory(this);
    }

    @Override
    protected void onPresenterPrepared(@NonNull LoginPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean onBackPressed(){
        return false;
    }

    private boolean isInputValid(){
        boolean mailResult;
        boolean passResult;
        String passPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$";
        Animation animation = AnimationUtils.loadAnimation(getActivity(),R.anim.edittext_shake_animation);
        mailResult = android.util.Patterns.EMAIL_ADDRESS.matcher(mailEdit.getText().toString().trim()).matches();
        passResult = Pattern.matches(passPattern,passEdit.getText().toString().trim());
        if(!mailResult){
            mailEdit.startAnimation(animation);
        }
        if(!passResult){
            passEdit.startAnimation(animation);
        }

        return mailResult && passResult;
    }

    @Override
    public void setCaptcha(Bitmap bm){}

    @Override
    public void showRegisterResult(int resultCode){}
}
