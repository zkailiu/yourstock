package com.zkliu.android.yourstock.mvp.base.backpress;

/**
 * Created by liuzekai on 13/05/2017.
 */
//http://www.jianshu.com/p/fff1ef649fc0
public interface FragmentBackHandler {
    boolean onBackPressed();
}
