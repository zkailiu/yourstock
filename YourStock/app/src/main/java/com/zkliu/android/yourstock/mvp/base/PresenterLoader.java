package com.zkliu.android.yourstock.mvp.base;

import android.content.Context;
import android.support.v4.content.Loader;
import android.util.Log;

/**
 * Created by liuzekai on 2017/1/10.
 */

public class PresenterLoader<T extends BasePresenter> extends Loader<T> {

    private final BasePresenterFactory<T> factory;
    private T presenter;

    public PresenterLoader(Context context, BasePresenterFactory<T> factory){
        super(context);
        this.factory = factory;
    }

    @Override
    protected void onStartLoading(){
        Log.i("loader","onStartLoading");

        // if we already own a presenter instance, simply deliver it.
        if(presenter != null){
            deliverResult(presenter);
            return;
        }
        //otherwise, force a load
        forceLoad();
    }

    @Override
    protected void onForceLoad(){
        Log.i("loader", "onForceLoad");

        //create the presenter using the factort
        presenter = factory.create();

        //deliver the result
        deliverResult(presenter);
    }

    @Override
    public void deliverResult(T data){
        super.deliverResult(data);
        Log.i("loader", "deliverResult");
    }

    @Override
    protected void onStopLoading(){
        Log.i("loader", "onStopLoading");
    }

    @Override
    protected void onReset(){
        Log.i("loader", "onReset");
        if(presenter != null){
            presenter.onDestroyed();
            presenter = null;
        }
    }
}
