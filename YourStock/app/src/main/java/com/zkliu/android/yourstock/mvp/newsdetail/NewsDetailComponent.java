package com.zkliu.android.yourstock.mvp.newsdetail;

import com.zkliu.android.yourstock.data.source.news.NewsRepositoryComponent;
import com.zkliu.android.yourstock.mvp.base.FragmentScoped;

import dagger.Component;

/**
 * Created by zekail on 8/6/2017.
 */
@FragmentScoped
@Component(modules = NewsDetailModule.class,
        dependencies = NewsRepositoryComponent.class)
public interface NewsDetailComponent {
    void inject(NewsDetailPresenterFactory newsDetailresenterFactory);
}
