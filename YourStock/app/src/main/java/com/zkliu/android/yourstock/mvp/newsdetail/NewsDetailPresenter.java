package com.zkliu.android.yourstock.mvp.newsdetail;

import android.support.annotation.NonNull;
import android.util.Log;

import com.zkliu.android.yourstock.data.source.news.NewsRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by zekail on 8/6/2017.
 */

public class NewsDetailPresenter implements NewsDetailContract.Presenter {

    @NonNull
    private final CompositeDisposable compositeDisposable;

    @NonNull
    private NewsDetailContract.View mNewsDetailView;

    @NonNull
    private final NewsRepository mNewsRepository;

    @Inject
    NewsDetailPresenter(NewsDetailContract.View newsdetailView, NewsRepository newsRepository){
        mNewsDetailView = newsdetailView;
        mNewsRepository = newsRepository;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewAttached(NewsDetailContract.View view){
        this.mNewsDetailView = view;
    }

    @Override
    public void onViewDetached() {
        mNewsDetailView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }

    @Override
    public void getNewsContent(String id, String category){
        mNewsDetailView.showProgress();
        mNewsRepository.getNewsDetailContent(id,category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(String content){
                        Log.i("detailcontent",content);
                        mNewsDetailView.showContent(content);
                    }
                    @Override
                    public void onError(Throwable e){
                        mNewsDetailView.hideProgress();
                    }
                    @Override
                    public void onComplete(){
                        mNewsDetailView.hideProgress();
                    }
                });
    }

    @Override
    public int getFontSize(){
        return mNewsRepository.getFontSizeFromSharedPref();
    }

    @Override
    public void saveFontSize(int size){
        mNewsRepository.saveFontSizeToSharedPref(size);
    }
}
