package com.zkliu.android.yourstock.widget.scaleImageView;

import android.view.View;

/**
 * Created by liuzekai on 01/07/2017.
 */

public interface OnViewTapListener {

    void onViewTap(View view, float x, float y);

}
