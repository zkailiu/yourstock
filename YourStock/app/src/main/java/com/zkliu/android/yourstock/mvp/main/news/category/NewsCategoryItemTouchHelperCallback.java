package com.zkliu.android.yourstock.mvp.main.news.category;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

/**
 * Created by zekail on 9/5/2017.
 */

public class NewsCategoryItemTouchHelperCallback extends ItemTouchHelper.Callback{

    private MoveAndDismissListener mListener;

    public NewsCategoryItemTouchHelperCallback(MoveAndDismissListener listener){
        this.mListener = listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder){
        final int position = viewHolder.getAdapterPosition();
        final int dragFlags = position == 0 ? 0 : (ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        final int swipeFlags = 0;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder start, RecyclerView.ViewHolder target){
        if(start.getItemViewType() != target.getItemViewType()){
            return false;
        }
        if(target.getAdapterPosition() !=0){
            mListener.onItemMove(start.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction){
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState){
        super.onSelectedChanged(viewHolder, actionState);
        if(actionState != ItemTouchHelper.ACTION_STATE_IDLE){
            ChoseCategoryAdapter.NewsCategoryChoseViewHolder holder = (ChoseCategoryAdapter.NewsCategoryChoseViewHolder)viewHolder;
            holder.setBackground(false);
        }
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder){
        super.clearView(recyclerView,viewHolder);
        ChoseCategoryAdapter.NewsCategoryChoseViewHolder holder = (ChoseCategoryAdapter.NewsCategoryChoseViewHolder)viewHolder;
        holder.setBackground(true);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
    }
}
