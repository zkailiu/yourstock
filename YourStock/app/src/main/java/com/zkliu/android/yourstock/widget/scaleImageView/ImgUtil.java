package com.zkliu.android.yourstock.widget.scaleImageView;

import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by liuzekai on 01/07/2017.
 */

class ImgUtil {

    static void checkZoomLevels(float minZoon, float midZoom, float maxZoom){
        if(minZoon >= midZoom){
            throw new IllegalArgumentException(
                    "Minimum zoom has to be less than Medium zoom. Call setMinimumZoom() with a more appropriate value");
        }else if(midZoom >= maxZoom){
            throw new IllegalArgumentException(
                    "Medium zoom has to be less than Maximum zoom. Call setMaximumZoom() with a more appropriate value");
        }
    }

    static boolean hasDrawable(ImageView imageView){
        return imageView.getDrawable() != null;
    }

    static boolean isSupportedScaleType(final ImageView.ScaleType scaleType){
        if(scaleType == null){
            return false;
        }
        switch (scaleType){
            case MATRIX:
                throw new IllegalStateException("Matrix scale type is not supported");
        }
        return true;
    }

    static int getPointerIndex(int action){
        return (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
    }
}
