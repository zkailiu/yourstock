package com.zkliu.android.yourstock.mvp.login;

import android.graphics.Bitmap;

import com.zkliu.android.yourstock.mvp.base.BasePresenter;

/**
 * Created by liuzekai on 06/07/2017.
 */

public interface LoginContract {

    interface View{
        void setCaptcha(Bitmap bm);

        void showRegisterResult(int resultCode);
    }

    interface Presenter extends BasePresenter<View> {

        void getCaptcha();

        void register(String mail, String password, String captcha, String name);

        void login(String mail, String password);
    }
}
