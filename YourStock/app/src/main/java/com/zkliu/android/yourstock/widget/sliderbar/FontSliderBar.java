package com.zkliu.android.yourstock.widget.sliderbar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by zekail on 22/6/2017.
 */

public class FontSliderBar extends View {

    private static final String TAG = "FONTSLIDETBAR";

    private static final int DEFAULT_BAR_COLOR = Color.LTGRAY;
    private static final float DEFAULT_BAR_WIDTH = 3.0f;

    private static final int DEFAULT_TEXT_COLOR = Color.LTGRAY;
    private static final int DEFAULT_TEXT_PADDING = 20;
    private static final int DEFAULT_TEXT_SIZE = 16;

    private static final int DEFAULT_THUMB_COLOR_NORMAL = 0xff33b5e5;
    private static final int DEFAULT_THUMB_COLOR_PRESSED = 0xff33b5e5;
    private static final float DEFAULT_THUMB_RADIUS = 20.0f;

    private static final int DEFAULT_TICK_COUNT = 3;
    private static final float DEFAULT_TICK_HEIGHT = 24.0f;

    private int clickIndex = -1;
    private boolean mAnimation = true;
    private ValueAnimator mAnimator;

    private Bar mBar;
    private int mBarColor = DEFAULT_BAR_COLOR;
    private float mBarWidth = DEFAULT_BAR_WIDTH;

    private int mCurrentIndex = 0;
    private int mDefaultWidth = 500;

    private OnSliderBarChangeListener mListener;

    private int mTextColor = DEFAULT_TEXT_COLOR;
    private int mTextPadding = DEFAULT_TEXT_PADDING;
    private int mTextSize = DEFAULT_TEXT_SIZE;

    private Thumb mThumb;
    private int mThumbColorNormal = DEFAULT_THUMB_COLOR_NORMAL;
    private int mThumbColorPressed = DEFAULT_THUMB_COLOR_PRESSED;
    private float mThumbRadius = DEFAULT_THUMB_RADIUS;

    private int mTickCount = DEFAULT_TICK_COUNT;
    private float mTickHeight = DEFAULT_TICK_HEIGHT;

    private OnViewClickListener mViewClick = null;
    private int startRawX;
    private int startRawY;

    public FontSliderBar(Context context){
        super(context);
    }

    public FontSliderBar(Context context, AttributeSet attributeSet){
        super(context,attributeSet);
    }

    public FontSliderBar(Context context, AttributeSet attrs, int defStyle){
        super(context,attrs,defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
        int width;
        int height;

        final int measureWidthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int measureHeighrMode = MeasureSpec.getMode(heightMeasureSpec);
        final int measureWidth = MeasureSpec.getSize(widthMeasureSpec);
        final int measureHeight = MeasureSpec.getSize(heightMeasureSpec);

        if(measureWidthMode == MeasureSpec.AT_MOST){
            width = measureWidth;
        }else if(measureWidthMode == MeasureSpec.EXACTLY){
            width = measureWidth;
        }else {
            width = mDefaultWidth;
        }

        if(measureHeighrMode == MeasureSpec.AT_MOST){
            height = Math.min(getMinHeight(), measureHeight);
        }else if(measureHeighrMode == MeasureSpec.EXACTLY){
            height = measureHeight;
        }else {
            height = getMinHeight();
        }

        setMeasuredDimension(width,height);
    }

    private int getMinHeight(){
        final float f = getFontHeight();
        return (int)(f + mTextPadding + mThumbRadius * 2);
    }

    private float getFontHeight(){
        Paint paint = new Paint();
        paint.setTextSize(mTextSize);
        paint.measureText("大");
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        float f = fontMetrics.descent - fontMetrics.ascent;
        return f;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        createBar();
        createThumbs();
        mThumb.setX(mBar.getCurrentX(mCurrentIndex));//getCurrentX 应该是 getCurrentX
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        mBar.draw(canvas);
        mThumb.draw(canvas);
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility){
        super.onVisibilityChanged(changedView, visibility);
        if(VISIBLE != visibility){
            stopAnimation();
        }
    }

    @Override
    protected void onDetachedFromWindow(){
        destroyResources();
        super.onDetachedFromWindow();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(!isEnabled() || isAnimationRunning()){
            return false;
        }
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                clickIndex = mBar.getClickIndex(event.getX());//getClickIndex 应该是 getClickIndex
                return onActionDown(event.getX(), event.getY());
            case MotionEvent.ACTION_MOVE:
                getParent().requestDisallowInterceptTouchEvent(true);
                return onActionMove(event.getX());
            case MotionEvent.ACTION_UP:
                int index = mBar.getClickIndex(event.getX());
                if(!(index == mCurrentIndex || index == -1 || index !=clickIndex)){
                    mViewClick.onClick(index);
                    moveThumbOnclick(mThumb, event.getX());
                    releaseThumb(mThumb);
                    break;
                }
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                return true;
        }
        getParent().requestDisallowInterceptTouchEvent(false);
        return onActionUp(event.getX(), event.getY());
    }

    public FontSliderBar setTickCount(int tickCount){
        if(isValidTickCount(tickCount)){
            mTickCount = tickCount;
            return FontSliderBar.this;
        }
        throw new IllegalArgumentException("tickCount less than 2; invalid tickCount.");
    }

    public FontSliderBar setTickHeight(float tickHeight){
        mTickHeight = tickHeight;
        return FontSliderBar.this;
    }

    public FontSliderBar setBarWidth(float width){
        mBarWidth = width;
        return FontSliderBar.this;
    }

    public FontSliderBar setSelectIndex(int index){
        mCurrentIndex = index;
        return FontSliderBar.this;
    }

    public FontSliderBar setBarColor(int color){
        mBarColor = color;
        return FontSliderBar.this;
    }

    public FontSliderBar setTextSize(int textSize){
        mTextSize = textSize;
        return FontSliderBar.this;
    }

    public FontSliderBar setTextColor(int textColor) {
        mTextColor = textColor;
        return FontSliderBar.this;
    }

    public FontSliderBar setTextPadding(int textPadding) {
        mTextPadding = textPadding;
        return FontSliderBar.this;
    }

    public FontSliderBar setThumbRadius(float thumbRadius) {
        mThumbRadius = thumbRadius;
        return FontSliderBar.this;
    }

    public FontSliderBar setThumbColorNormal(int thumbColorNormal) {
        mThumbColorNormal = thumbColorNormal;
        return FontSliderBar.this;
    }

    public FontSliderBar setThumbColorPressed(int thumbColorPressed) {
        mThumbColorPressed = thumbColorPressed;
        return FontSliderBar.this;
    }

    public FontSliderBar setThumbIndex(int currentIndex){
        if(indexOutOfRange(currentIndex)){
            throw new IllegalArgumentException("A thumb index is out of bounds. Check that it is between 0 and mTickCount - 1");
        }
        if(mCurrentIndex != currentIndex){
            mCurrentIndex = currentIndex;
            if(mListener != null){
                mListener.onIndexChanged(this, mCurrentIndex);
            }
        }
        return FontSliderBar.this;
    }

    public FontSliderBar withAnimation(boolean animation){
        mAnimation = animation;
        return FontSliderBar.this;
    }

    public void apply(){
        createThumbs();
        createBar();
        requestLayout();
        invalidate();
    }

    public void setItemSelected(int index, boolean z) {
        this.mBar.setSelectedItemIndex(index);
        if (!z) {
            this.mThumb.setX(this.mBar.getCurrentXPosition(index));
        }
        invalidate();
    }

    public int getCurrentIndex(){
        return mCurrentIndex;
    }

    private void createBar(){
        mBar = new Bar(getXCoordinate(), getYCoordinate(), getBarLength(), mTickCount, mTickHeight,
                mBarWidth, mBarColor, mTextColor, mTextSize, mTextPadding);
        mBar.setSelectedItemIndex(mCurrentIndex);
    }

    private void createThumbs(){
        mThumb = new Thumb(getXCoordinate(), getYCoordinate(), mThumbColorNormal,
                mThumbColorPressed, mThumbRadius);
    }

    private float getXCoordinate(){
        return mThumbRadius;
    }

    private float getYCoordinate(){
        return getHeight() - mThumbRadius;
    }

    private float getBarLength(){
        return getWidth() - 2.0f * getXCoordinate();
    }

    private boolean indexOutOfRange(int thumbIndex){
        return (thumbIndex < 0 || thumbIndex >= mTickCount);
    }

    private boolean isValidTickCount(int tickCount){
        return tickCount > 1;
    }

    private boolean onActionDown(float x, float y){
        if(!mThumb.isPressed() && mThumb.isInTargetZone(x,y)){
            pressThumb(mThumb);
        }
        return true;
    }

    private boolean onActionMove(float x){
        if(mThumb.isPressed()){
            moveThumb(mThumb, x);
        }
        return true;
    }

    private boolean onActionUp(float x, float y){
        if(mThumb.isPressed()){
            releaseThumb(mThumb);
        }
        return true;
    }

    private void pressThumb(Thumb thumb){
        thumb.press();
        invalidate();
    }

    private void releaseThumb(Thumb thumb){
        final int tempIndex = mBar.getNearestTickIndex(thumb);
        if(tempIndex != mCurrentIndex){
            mCurrentIndex = tempIndex;
            if(null != mListener){
                mListener.onIndexChanged(this, mCurrentIndex);
            }
        }
        float start = thumb.getX();
        float end = mBar.getNearestTickCoordinate(thumb);
        if(mAnimation){
            startAnimation(thumb, start, end);
        }else {
            thumb.setX(end);
            invalidate();
        }
        thumb.release();
    }

    private void startAnimation(final Thumb thumb, float start, float end){
        stopAnimation();
        mAnimator = ValueAnimator.ofFloat(start,end);
        mAnimator.setDuration(80);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                final float x = (Float)valueAnimator.getAnimatedValue();
                thumb.setX(x);
                invalidate();
            }
        });
        mAnimator.start();
    }

    private boolean isAnimationRunning(){
        if(null != mAnimator && mAnimator.isRunning()){
            return true;
        }
        return false;
    }

    private void destroyResources() {
        stopAnimation();
        if (mBar != null) {
            mBar.destroyResources();
            mBar = null;
        }
        if (mThumb != null) {
            mThumb.destroyResources();
            mThumb = null;
        }
    }

    private void stopAnimation() {
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
    }

    private void moveThumb(Thumb thumb, float x){
        if(x<mBar.getLeftX() || x>mBar.getRightX()){
            //nothing
        }else {
            thumb.setX(x);
            invalidate();
        }
    }

    private void moveThumbOnclick(Thumb thumb, float x){
        if(x<mBar.getLeftX() || x>mBar.getRightX()){
            if(x < mBar.getLeftX() && x >= mBar.getLeftX() - mThumbRadius){
                x = mBar.getLeftX();
            }else if(x>mBar.getRightX() && x< mBar.getRightX() + mThumbRadius){
                x = mBar.getRightX();
            }else {
                return;
            }
        }
        thumb.setX(x);
        invalidate();
    }

    public interface OnSliderBarChangeListener {
        void onIndexChanged(FontSliderBar fontSliderBar, int index);
    }

    public FontSliderBar setOnSliderBarChangeListener(OnSliderBarChangeListener listener){
        mListener = listener;
        return FontSliderBar.this;
    }

    public interface OnViewClickListener{
        void onClick(int index);
    }

    public void setOnViewClickListener(OnViewClickListener listener){
        mViewClick = listener;
    }
}
