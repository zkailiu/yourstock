package com.zkliu.android.yourstock.mvp.login;

import com.zkliu.android.yourstock.data.source.users.UsersRepositoryComponent;
import com.zkliu.android.yourstock.mvp.base.FragmentScoped;

import dagger.Component;

/**
 * Created by liuzekai on 06/07/2017.
 */


@FragmentScoped
@Component(modules = LoginModule.class,
dependencies = UsersRepositoryComponent.class)
public interface LoginComponent {
    void inject(LoginPresenterFactory loginPresenterFactory);
}