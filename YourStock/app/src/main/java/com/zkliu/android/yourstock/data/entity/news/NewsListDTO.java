package com.zkliu.android.yourstock.data.entity.news;

import com.zkliu.android.yourstock.data.entity.MapperDTtoV;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuzekai on 02/04/2017.
 */

public class NewsListDTO implements MapperDTtoV<List<News>> {

    public List<NewsDTO> newsList;

    @Override
    public List<News> transform(){
        List<News> list = new ArrayList<>();
        for(NewsDTO dto : newsList){
            list.add(dto.transform());
        }
        return list;
    }
}
