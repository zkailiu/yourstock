package com.zkliu.android.yourstock.widget.swipe;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zkliu.android.yourstock.R;

/**
 * Created by liuzekai on 14/04/2017.
 */

public class SwipeFooter extends FrameLayout implements CustomSwipeLayout.SwipeTrigger, CustomSwipeLayout.SwipeLoadMoreTrigger {

    private ImageView ivArrow;

    private ImageView ivSuccess;

    private TextView tvLoadMore;

    private ImageView ivProgress;


    private Animation rotateUp;

    private Animation rotateDown;

    private Animation rotated360;

    private boolean rotated = false;

    private int mFooterHeight;
    public SwipeFooter(Context context) {
        this(context, null);
    }

    public SwipeFooter(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeFooter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mFooterHeight = getResources().getDimensionPixelOffset(R.dimen.swipe_footer_height);
        rotateUp = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_up);
        rotateDown = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_down);
        rotated360 = AnimationUtils.loadAnimation(context,R.anim.swipe_header_rotate_360_degree);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvLoadMore = (TextView) findViewById(R.id.loadMoreTv);
        ivArrow = (ImageView)findViewById(R.id.footerArrowIv);
        ivSuccess = (ImageView)findViewById(R.id.footerSuccessIv);
        ivProgress = (ImageView) findViewById(R.id.footerLoadMoreIv);
    }

    @Override
    public void onLoadMore() {
        tvLoadMore.setText(R.string.loading_more);
        ivSuccess.setVisibility(GONE);
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        ivProgress.setVisibility(VISIBLE);
        ivProgress.startAnimation(rotated360);

    }

    @Override
    public void onPrepare() {
    }

    @Override
    public void onMove(int y, boolean isComplete, boolean automatic, boolean hideText) {
        if(!hideText){
            if(!isComplete){
                ivArrow.setVisibility(VISIBLE);
                ivSuccess.setVisibility(GONE);
                ivProgress.clearAnimation();
                ivProgress.setVisibility(GONE);
                if( -y > mFooterHeight){
//use from prestorge
                    tvLoadMore.setText(R.string.release_to_load_more);
                    if(!rotated){
                        ivArrow.clearAnimation();
                        ivArrow.startAnimation(rotateUp);
                        rotated = true;
                    }
                }else if( -y < mFooterHeight){
                    if(rotated){
                        ivArrow.clearAnimation();
                        ivArrow.startAnimation(rotateDown);
                        rotated = false;
                    }
                    tvLoadMore.setText(R.string.swipe_to_load_more);
                }
            }else {
                tvLoadMore.setText("RETURNING");
            }
        }
    }

    @Override
    public void onRelease() {
        tvLoadMore.setText("Release");
    }

    @Override
    public void onComplete(boolean isLoadMoreSuccess) {
        rotated = false;
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        ivSuccess.setVisibility(VISIBLE);
        ivProgress.clearAnimation();
        ivProgress.setVisibility(GONE);
        if(isLoadMoreSuccess){
            tvLoadMore.setText(R.string.load_more_complete_success);
        }else {
            tvLoadMore.setText(R.string.load_more_complete_fail);
        }
    }

    @Override
    public void onReset() {
        rotated = false;
        ivArrow.clearAnimation();
        ivArrow.setVisibility(GONE);
        ivProgress.clearAnimation();
        ivProgress.setVisibility(GONE);
        ivSuccess.setVisibility(GONE);

    }
}
