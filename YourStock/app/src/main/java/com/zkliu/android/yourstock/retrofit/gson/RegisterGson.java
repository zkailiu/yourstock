package com.zkliu.android.yourstock.retrofit.gson;

/**
 * Created by liuzekai on 17/07/2017.
 */

public class RegisterGson {
    /**
     * balance : 0
     * init_money_received : 0
     * mail : la
     * name : ba
     * result_code : 1
     */

    private int result_code;

    public int getResult_code() {
        return result_code;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }
}
