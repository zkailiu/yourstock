package com.zkliu.android.yourstock.mvp.photodetail;

import android.support.annotation.NonNull;


import com.zkliu.android.yourstock.data.source.news.NewsRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by zekail on 28/6/2017.
 */

public class PhotoDetailPresenter implements PhotoDetailContract.Presenter {

    @NonNull
    private final CompositeDisposable compositeDisposable;

    @NonNull
    private PhotoDetailContract.View mPhotoDetailView;

    @NonNull
    private final NewsRepository mNewsRepository;

    @Inject
    PhotoDetailPresenter(PhotoDetailContract.View photodetailView, NewsRepository newsRepository){
        mPhotoDetailView = photodetailView;
        mNewsRepository = newsRepository;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewAttached(PhotoDetailContract.View view){
        this.mPhotoDetailView = view;
    }

    @Override
    public void onViewDetached() {
        mPhotoDetailView = null;
    }

    @Override
    public void onDestroyed() {
        // Nothing to clean up
    }

    @Override
    public List<String> getImgList(String id){
        return mNewsRepository.getImgList(id);
    }

    @Override
    public void saveImg(final String url){
        mNewsRepository.saveImg(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>(){
                    @Override
                    public void onStart(){
                    }
                    @Override
                    public void onNext(String result){
                        mPhotoDetailView.showSaveResult(result);
                    }
                    @Override
                    public void onError(Throwable e){
                        if(e.getMessage()!=null){
                            mPhotoDetailView.showSaveResult(e.getMessage());
                        }
                    }
                    @Override
                    public void onComplete(){

                    }
                });

    }
}
