package com.zkliu.android.yourstock.mvp.main.news.sub;


import dagger.Module;
import dagger.Provides;

/**
 * Created by zekail on 4/5/2017.
 */
@Module
public class NewsSubModule {

    private NewsSubContract.View mView;

    public NewsSubModule(NewsSubContract.View view){this.mView = view;}

    @Provides
    NewsSubContract.View provideNewsSubContractView(){
        return mView;
    }
}
