package com.zkliu.android.yourstock.mvp.main.news.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by liuzekai on 05/04/2017.
 */

public class NewsListPicassoTarget implements Target {

    public ImageView mImageView;
    private Context mContext;

    public NewsListPicassoTarget(ImageView imageView, Context context){
        this.mImageView = imageView;
        this.mContext = context;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom){
        Drawable bitmapDrawable = new BitmapDrawable(mContext.getResources(),bitmap);
        mImageView.setImageDrawable(bitmapDrawable);
    }

    @Override
    public void onBitmapFailed(Drawable drawable){
        //do something
        mImageView.setImageDrawable(drawable);
    }

    @Override
    public void onPrepareLoad(Drawable drawable) {
        // Do nothing, default_background manager has its own transitions
        mImageView.setImageDrawable(drawable);
    }
}
