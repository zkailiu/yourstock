package com.zkliu.android.yourstock.mvp.main.news;

import dagger.Module;
import dagger.Provides;

/**
 * Created by liuzekai on 22/01/2017.
 */
@Module
public class NewsModule {

    private NewsContract.View mView;

    public NewsModule(NewsContract.View view){
        this.mView = view;
    }

    @Provides
    NewsContract.View provideNewsContractView(){
        return mView;
    }
}
