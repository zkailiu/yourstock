package com.zkliu.android.yourstock.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.zkliu.android.yourstock.util.Constants;
import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.util.DensityUtil;

/**
 * Created by liuzekai on 2016/12/26.
 */

public class BezierCircle extends View {

    private Context context;
    private static final float sMagicNumber = 0.55228475f;
    private Paint mPaint;
    private Path mPath;
    private float mPrevX;
    private float mPrevY;
    private int radius ;

    public BezierCircle(Context context){
        super(context);
        this.context = context;
        init();
    }

    public BezierCircle(Context context, AttributeSet attrs){
        super(context, attrs);
        this.context = context;
        init();
    }

    public BezierCircle(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    private void init(){
        mPaint = new Paint();
        mPaint.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mPaint.setStyle(Paint.Style.FILL);
        mPath = new Path();
        updatePath(0, 0);
    }

    public void updatePath(float x, float y){
        radius = DensityUtil.dp2px(context, Constants.RADIUS);
        float distance = distance(mPrevX, mPrevY, x, y);
        float longRadius = radius + distance * 2.5f;
        float shortRadius = radius - distance * 0.1f;

        mPath.reset();

        mPath.lineTo(0, longRadius);
        mPath.cubicTo(radius * sMagicNumber, longRadius, radius, radius * sMagicNumber, radius, 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, longRadius);
        mPath.cubicTo(-radius * sMagicNumber, longRadius, -radius, radius * sMagicNumber, -radius , 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, -shortRadius);
        mPath.cubicTo(radius * sMagicNumber, -shortRadius, radius, -radius * sMagicNumber, radius, 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, -shortRadius);
        mPath.cubicTo(-radius * sMagicNumber, -shortRadius, -radius, -radius * sMagicNumber, -radius, 0);
        mPath.lineTo(0, 0);

        invalidate();
    }

    public void updatePathrotate(float fraction){
        radius = DensityUtil.dp2px(context, Constants.RADIUS);

        mPath.reset();
        mPaint.setAntiAlias(true);

        mPath.lineTo(0, radius);
        mPath.cubicTo(radius * (sMagicNumber + fraction * (1 - sMagicNumber)), radius, radius, radius * (sMagicNumber + fraction * (1 - sMagicNumber)), radius, 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, radius);
        mPath.cubicTo(-radius * (sMagicNumber + fraction * (1 - sMagicNumber)), radius, -radius, radius * (sMagicNumber + fraction * (1 - sMagicNumber)), -radius , 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, -radius);
        mPath.cubicTo(radius * (sMagicNumber + fraction * (1 - sMagicNumber)), -radius, radius, -radius * (sMagicNumber + fraction * (1 - sMagicNumber)), radius, 0);
        mPath.lineTo(0, 0);

        mPath.lineTo(0, -radius);
        mPath.cubicTo(-radius * (sMagicNumber + fraction * (1 - sMagicNumber)), -radius, -radius, -radius * (sMagicNumber + fraction * (1 - sMagicNumber)), -radius, 0);
        mPath.lineTo(0, 0);

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas){
        int centerX = getMeasuredWidth() >>1 ;
        int centerY = getMeasuredHeight()>>1 ;

        canvas.save();
        canvas.translate(centerX, centerY);
        canvas.drawPath(mPath, mPaint);
        canvas.restore();
    }

    public static float distance(float x1, float y1, float x2, float y2){
        return (float)Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

}
