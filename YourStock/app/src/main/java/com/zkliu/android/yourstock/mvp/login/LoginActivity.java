package com.zkliu.android.yourstock.mvp.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.zkliu.android.yourstock.R;
import com.zkliu.android.yourstock.mvp.base.backpress.BackHandlerHelper;
import com.zkliu.android.yourstock.util.ActivityUtils;

/**
 * Created by liuzekai on 06/07/2017.
 */

public class LoginActivity extends AppCompatActivity {

    public final static String LOGIN_FRAG = "LOGIN_FRAG";
    public final static String REGISTER_FRAG = "REGISTER_FRAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        LoginFragment loginFragment = LoginFragment.newInstance();
        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),loginFragment,R.id.loginFrame,LOGIN_FRAG);

        RegisterFragment registerFragment = RegisterFragment.newInstance();
        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),registerFragment,R.id.loginFrame, REGISTER_FRAG);
        ActivityUtils.switchBetweenFragment(getSupportFragmentManager(),getSupportFragmentManager().findFragmentByTag(REGISTER_FRAG),
                getSupportFragmentManager().findFragmentByTag(LOGIN_FRAG));

    }

    @Override
    public void onBackPressed(){
        if(!BackHandlerHelper.handleBackPress(this)){
            this.finish();
            overridePendingTransition(R.anim.slide_backward_in,
                    R.anim.slide_backward_out);
        }
    }
}
