package com.zkliu.android.yourstock.mvp.main.news.category;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by zekail on 8/5/2017.
 */

public class NewsCategorySpaceDecoration extends RecyclerView.ItemDecoration {
    int mSpace;

    public NewsCategorySpaceDecoration(int space){
        this.mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
        int pos = parent.getChildAdapterPosition(view);

        outRect.left = mSpace;
        outRect.top = 0;
        outRect.right = mSpace;
        outRect.bottom = mSpace;
    }
}
