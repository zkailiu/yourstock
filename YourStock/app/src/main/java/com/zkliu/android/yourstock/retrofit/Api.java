package com.zkliu.android.yourstock.retrofit;

/**
 * Created by liuzekai on 09/04/2017.
 */

public class Api {

    // Base API
    public static final String API_BASE = "http://192.168.31.95:5000/";
    //public static final String API_BASE = "http://172.27.17.10:5000/";
    //http://0.0.0.0:5000/api/news/finance

    // Get headline news list and non-headline news id list
    public static final String NEWS = "api/news/";
    public static final String NEWS_DETAIL = "api/news/detail/";

    //register and update user info
    public static final String USERS = "api/users/";//get - get capatch image; post - register

}
//https://www.v2ex.com/t/118049
//https://github.com/realpython/flask-registration/blob/master/project/user/views.py
//https://github.com/PrettyPrinted/mongodb-user-login
//http://python.jobbole.com/81410/