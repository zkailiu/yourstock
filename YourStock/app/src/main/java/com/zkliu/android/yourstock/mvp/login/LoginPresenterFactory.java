package com.zkliu.android.yourstock.mvp.login;

import com.zkliu.android.yourstock.app.StockApplication;
import com.zkliu.android.yourstock.mvp.base.BasePresenterFactory;

import javax.inject.Inject;

/**
 * Created by liuzekai on 06/07/2017.
 */

public class LoginPresenterFactory implements BasePresenterFactory<LoginPresenter> {

    private final LoginContract.View fragment;
    public LoginPresenterFactory(LoginContract.View loginFragment){
        fragment = loginFragment;
    }

    @Inject
    LoginPresenter presenter;

    @Override
    public LoginPresenter create(){
        DaggerLoginComponent.builder()
                .loginModule(new LoginModule(fragment))
                .usersRepositoryComponent(StockApplication.getUsersRepositoryComponent())
                .build().inject(this);
        return presenter;
    }
}
