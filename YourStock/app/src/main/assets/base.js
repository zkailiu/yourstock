function isElementinViewport (element) {
    var offset = 100;
    var coords = element.getBoundingClientRect();
    if (coords.top < 0) {
        return true;
    } else {
        return ((coords.top >= 0 && coords.left >= 0 && coords.top) <= (window.innerHeight || document.body.clientHeight) + offset);
    }
}
function _pollImages () {
    var imageList = document.getElementsByTagName("img");
    for(var i=0; i<imageList.length; i++){
       var image = imageList[i];
       if (image && image.getAttribute("src")=="file:///android_asset/images/placeholder.png" && isElementinViewport(image)) {
       image.setAttribute('src', image.getAttribute("href"));
    }
  }
}
function initializeImageLazyload () {
    _pollImages();
    document.removeEventListener('scroll', _pollImages, false);
    document.addEventListener('scroll', _pollImages, false);
}
function initPage(){
    setTimeout(function(){
      initializeImageLazyload();
      imageInit();
    },200);
  }

document.addEventListener("DOMContentLoaded",initPage,false);

function imageInit(){
    var imageList = document.getElementsByTagName("img");
    for(var i=0; i<imageList.length; i++){
            var image = imageList[i];
            image.style.width = '100%';
            image.style.height = 'auto';
            image.onclick = function(){
                window.jsFinance.showImageDetail(this.src);
                return false;
            }
        }
}

function changeSizeForIndex(index) {
    if(index == "" || index == undefined || index == null){
		return ;
	}
    var articleContent = document.getElementById('articleContent');
    var n = parseInt(index);
    if(n==0){
           articleContent.style.fontSize = '16px';
           articleContent.style.lineHeight = '27px';
    }else  if(n==1){
        articleContent.style.fontSize = '18px';
         articleContent.style.lineHeight = '30px';
    } else if(n==2){
        articleContent.style.fontSize = '20px';
         articleContent.style.lineHeight = '32px';
    } else {
        articleContent.style.fontSize = '22px';
         articleContent.style.lineHeight = '35px';
    }
 }